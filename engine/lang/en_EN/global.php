<?php
lang::$langdata['global']['send'] = 'Send';
lang::$langdata['global']['login'] = 'Login';
lang::$langdata['global']['forum'] = 'Forum';
lang::$langdata['global']['registration'] = 'Registration';
lang::$langdata['global']['download_app'] = 'Download app';
lang::$langdata['global']['discussed'] = 'Discussed photos';
lang::$langdata['global']['contact'] = 'Contact';
lang::$langdata['global']['panel'] = 'Panel';
lang::$langdata['global']['start thinking'] = 'Start Thinking';
lang::$langdata['global']['map'] = 'Map';
lang::$langdata['global']['thinkspot'] = 'ThinkSpot';
lang::$langdata['global']['earthspot'] = 'EarthSpot';
lang::$langdata['global']['i am new'] = 'I am new';
lang::$langdata['global']['terms of use'] = 'Terms of use';
lang::$langdata['global']['more about earthinks'] = 'More about earthinks';
lang::$langdata['global']['create account'] = 'Create account';

lang::$langdata['global']['month01'] = 'January';
lang::$langdata['global']['month02'] = 'February';
lang::$langdata['global']['month03'] = 'March';
lang::$langdata['global']['month04'] = 'April';
lang::$langdata['global']['month05'] = 'May';
lang::$langdata['global']['month06'] = 'June';
lang::$langdata['global']['month07'] = 'July';
lang::$langdata['global']['month08'] = 'August';
lang::$langdata['global']['month09'] = 'September';
lang::$langdata['global']['month10'] = 'October';
lang::$langdata['global']['month11'] = 'November';
lang::$langdata['global']['month12'] = 'December';

lang::$langdata['global']['day1'] = 'Monday';
lang::$langdata['global']['day2'] = 'Tuesday';
lang::$langdata['global']['day3'] = 'Wednesday';
lang::$langdata['global']['day4'] = 'Thursday';
lang::$langdata['global']['day5'] = 'Friday';
lang::$langdata['global']['day6'] = 'Saturday';
lang::$langdata['global']['day7'] = 'Sunday';

?>
