<?php
lang::$langdata['error']['no_password'] = 'Please input password';
lang::$langdata['error']['no_login'] = 'Please input login';
lang::$langdata['error']['mail_format_error'] = 'Mail format error';
lang::$langdata['error']['birthday_format_error'] = 'Birthday format error. Required like 10/30/2000 or 06-10-2011';
lang::$langdata['error']['captcha_format_error'] = 'Captcha not correct';
lang::$langdata['error']['pass_no_match'] = 'Passwords don\'t match';
?>