<?php
lang::$langdata['user']['login'] = 'Login';
lang::$langdata['user']['password'] = 'Password';
lang::$langdata['user']['mail'] = 'Mail';
lang::$langdata['user']['password_repeat'] = 'Repeat password';
lang::$langdata['user']['name'] = 'Name';
lang::$langdata['user']['surname'] = 'Surname';
lang::$langdata['user']['birthday'] = 'Birthday';
lang::$langdata['user']['question'] = 'Question';
lang::$langdata['user']['captcha'] = 'Captcha';
lang::$langdata['user']['robots protection'] = 'Robots protection: type the characters you see in the picture below';
lang::$langdata['user']['Forgot password'] = 'Forgot password';
lang::$langdata['user']['Change password'] = 'Change password';
lang::$langdata['user']['Personal'] = 'Personal';
lang::$langdata['user']['remember me'] = 'Remember me';
lang::$langdata['user']['create account'] = 'Create account';
lang::$langdata['user']['i accept'] = 'I accept ';
?>