<?php
lang::$langdata['photo']['photo'] = 'Photo';
lang::$langdata['photo']['location'] = 'Location';
lang::$langdata['photo']['description'] = 'Description';
lang::$langdata['photo']['author'] = 'Author';
lang::$langdata['photo']['added'] = 'Added';
lang::$langdata['photo']['photo name'] = 'Photo name';
lang::$langdata['photo']['comment'] = 'Comment';
lang::$langdata['photo']['reply'] = 'Reply';
lang::$langdata['photo']['latest members'] = 'Latest members';
lang::$langdata['photo']['personal page'] = 'Personal<br>page';
?>