<?php
lang::$langdata['error']['no_password'] = 'Voer het wachtwoord aub';
lang::$langdata['error']['no_login'] = 'Voer het login aub';
lang::$langdata['error']['mail_format_error'] = 'Mail formaat fout';
lang::$langdata['error']['birthday_format_error'] = 'Verjaardag formaat fout. Vereist: 10/30/2000 of 06-10-2011';
lang::$langdata['error']['captcha_format_error'] = 'Captcha niet correct';
lang::$langdata['error']['pass_no_match'] = 'Wachtwoorden komen niet overeen';
?>