<?php
lang::$langdata['global']['send'] = 'Stuur';
lang::$langdata['global']['login'] = 'Login';
lang::$langdata['global']['forum'] = 'Forum';
lang::$langdata['global']['registration'] = 'Registratie';
lang::$langdata['global']['download_app'] = 'Download app';
lang::$langdata['global']['discussed'] = 'Fotos onder discussie';
lang::$langdata['global']['contact'] = 'Contact';
lang::$langdata['global']['panel'] = 'Paneel';
lang::$langdata['global']['start thinking'] = 'Start Denken';
lang::$langdata['global']['map'] = 'Kaart ';
lang::$langdata['global']['thinkspot'] = 'ThinkSpot';
lang::$langdata['global']['earthspot'] = 'EarthSpot';
lang::$langdata['global']['i am new'] = 'Ik ben Nieuw';
lang::$langdata['global']['terms of use'] = 'Voorwaarden';
lang::$langdata['global']['more about earthinks'] = 'Meer over earthinks';
lang::$langdata['global']['create account'] = 'Profiel Maken';

lang::$langdata['global']['month01'] = 'Januari';
lang::$langdata['global']['month02'] = 'Februari';
lang::$langdata['global']['month03'] = 'Maart';
lang::$langdata['global']['month04'] = 'April';
lang::$langdata['global']['month05'] = 'Mei';
lang::$langdata['global']['month06'] = 'Juni';
lang::$langdata['global']['month07'] = 'Juli';
lang::$langdata['global']['month08'] = 'Augustus';
lang::$langdata['global']['month09'] = 'September';
lang::$langdata['global']['month10'] = 'Oktober';
lang::$langdata['global']['month11'] = 'November';
lang::$langdata['global']['month12'] = 'December';

lang::$langdata['global']['day1'] = 'Maandag';
lang::$langdata['global']['day2'] = 'Dinsdag';
lang::$langdata['global']['day3'] = 'Woensdag';
lang::$langdata['global']['day4'] = 'Donderdag';
lang::$langdata['global']['day5'] = 'Vrijdag';
lang::$langdata['global']['day6'] = 'Zaterdag';
lang::$langdata['global']['day7'] = 'Zondag';

?>
