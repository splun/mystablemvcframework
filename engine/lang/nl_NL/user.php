<?php
lang::$langdata['user']['login'] = 'Login';
lang::$langdata['user']['password'] = 'Wachtwoord';
lang::$langdata['user']['mail'] = 'Mail';
lang::$langdata['user']['password_repeat'] = 'Herhaal wachtwoord';
lang::$langdata['user']['name'] = 'Naam';
lang::$langdata['user']['surname'] = 'Achternaam';
lang::$langdata['user']['birthday'] = 'Verjaardag';
lang::$langdata['user']['question'] = 'Vraag';
lang::$langdata['user']['captcha'] = 'Captcha';
lang::$langdata['user']['robots protection'] = 'Robots bescherming: Typ de tekens die u ziet in de afbeelding hieronder';
lang::$langdata['user']['Forgot password'] = 'Wachtwoord vergeten';
lang::$langdata['user']['Change password'] = 'Wijzig wachtwoord';
lang::$langdata['user']['Personal'] = 'Persoonlijk';
lang::$langdata['user']['remember me'] = 'Onthoud mij';
lang::$langdata['user']['create account'] = 'Maak een account';
lang::$langdata['user']['i accept'] = 'Ik accepteer';
?>