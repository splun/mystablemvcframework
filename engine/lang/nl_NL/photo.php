<?php
lang::$langdata['photo']['photo'] = 'Foto';
lang::$langdata['photo']['location'] = 'Locatie';
lang::$langdata['photo']['description'] = 'Beschrijving';
lang::$langdata['photo']['author'] = 'Auteur';
lang::$langdata['photo']['added'] = 'Toegevoegd';
lang::$langdata['photo']['photo name'] = 'Foto naam';
lang::$langdata['photo']['comment'] = 'Commentaar';
lang::$langdata['photo']['reply'] = 'Antwoord';
lang::$langdata['photo']['latest members'] = 'Laatste leden';
lang::$langdata['photo']['personal page'] = 'Persoonlijk pagina';
?>