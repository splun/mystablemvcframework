<?php
class dateloc{
    function date($timestamp){
        $str = date('j ', $timestamp).lang::getStr('global', 'month' . date('m', $timestamp), 'month' . date('m', $timestamp)).date('  Y ', $timestamp);
        return $str;
    }
    function dateTime($timestamp){
        $str = date('j ', $timestamp).lang::getStr('global', 'month' . date('m', $timestamp), 'month' . date('m', $timestamp)).date('  Y | H:i', $timestamp);
        return $str;
    }
}

?>
