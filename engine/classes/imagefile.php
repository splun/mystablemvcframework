<?php

class imageFile {

    var $file = null;

    function imageFile($adress) {
        $this->file = $adress;
    }

    function imageresize($outfile, $neww, $newh, $quality) {
        $im = imagecreatefromstring(file_get_contents($this->file));
        $k1 = $neww / imagesx($im);
        $k2 = $newh / imagesy($im);
        $k = $k1 > $k2 ? $k2 : $k1;
        $w = intval(imagesx($im) * $k);
        $h = intval(imagesy($im) * $k);
        $im1 = imagecreatetruecolor($w, $h);
        imagecopyresampled($im1, $im, 0, 0, 0, 0, $w, $h, imagesx($im), imagesy($im));
        imagejpeg($im1, $outfile, $quality);
        imagedestroy($im);
        imagedestroy($im1);
    }

    function save($newName, $path) {
        $nm = basename($newName);
        $image = imagecreatefromstring(file_get_contents($newName));
        $exif = exif_read_data($newName);
        if (!empty($exif['Orientation'])) {
            switch ($exif['Orientation']) {
                case 8:
                    $image = imagerotate($image, 90, 0);
                    imagejpeg($image, $newName);
                    break;
                case 3:
                    $image = imagerotate($image, 180, 0);
                    imagejpeg($image, $newName);
                    break;
                case 6:
                    $image = imagerotate($image, -90, 0);
                    imagejpeg($image, $newName);
                    break;
            }
        }
        $this->imageresize($path . 'files/thumbs/' . $nm, 200, 200, 100);
    }

}

?>