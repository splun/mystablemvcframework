<?php

class Db_Mysql {

    private $connection;
    private $user = '';
    private $password = '';
    private $name = '';
    private $host = '';

    function __construct() {
        $this->connect();
    }

    function Db_Mysql() {
        $this->connect();
    }

    private function connect() {
        $this->connection = mysql_connect($this->host, $this->user, $this->password) or die('<br>Error: ' . mysql_error());
        mysql_select_db($this->name) or die('<br>Error: ' . mysql_error());
        $this->query('SET NAMES `utf8`');
    }

    function explain($text) {
        $re = mysql_query('EXPLAIN ' . $text);
        $re = mysql_fetch_array($re);
        var_dump($re);
    }

    function query($text) {
        $res = mysql_query($text) or die('<br>Query: ' . $text . '<br>Error: ' . mysql_error());
        return $res;
    }

}

?>
