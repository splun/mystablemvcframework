<?php

class lang {

    protected static $instance;
    private static $langdata;
    static $lang;

    private function lang() {
        
    }
    
    static function getLang(){
        if (self::$lang == '') {
            if (ISSET($_SESSION['lang'])) {
                self::$lang = $_SESSION['lang'];
            } else {
                preg_match('/^\w{2}/', $_SERVER['HTTP_ACCEPT_LANGUAGE'], $matches);
                switch (strtolower($matches[0])) {
                    case "en":
                        self::$lang = "en_EN";
                        break;
                    case "nl":
                        self::$lang = "nl_NL";
                        break;
                    default:
                        self::$lang = "en_EN";
                        break;
                }
            }
        }
        return self::$lang;
    }

    private function __construct() {
        
    }

    function getInstance($type, $val, $default) {
        if (is_null(self::$instance)) {
            self::$instance = new Singleton;
        }
        return self::$instance;
    }

    static function getStr($type, $val, $default) {
        $lang = lang::getLang();
        if (isset(lang::$langdata[$type])) {
            if (isset(lang::$langdata[$type][$val])) {
                //$default = '<->'.lang::$langdata[$type][$val].'<->';
                $default = lang::$langdata[$type][$val];
            } else {
                //$default = 'str ' . $type . ' ' . $val . ' null. ' . $default;
            }
        } else {
            $pat = PATH_CORE . 'lang' . DS . self::$lang . DS . $type . '.php';
            if (file_exists($pat)) {
                include_once $pat;
                $default = lang::getStr($type, $val, $default);
            } else {
                //$default = '-> ' . $type . ' ' . $val . ' ' . $default . ' <-';
            }
        }
        return $default;
    }

    static function str($type, $val, $default) {
        echo lang::getStr($type, $val, $default);
    }

}

?>