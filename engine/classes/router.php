<?php

class Router {

    public $path;
    private $args = array();

    function setPath($path) {
        //$path = trim($path, '/\\');
        if (!is_dir($path)) {
            throw new Exception('Invalid controller path: `' . $path . '`');
        }
        $this->path = $path;
    }

    function delegate() {
        $this->getController($file, $controller, $action, $args, $list);
        $class = 'Controllers_' . $controller;
        $view = $controller;
        ob_start();
        $data = data::init();
        $data->set('controller', new $class());
        $data->set('args', $args);
        if (ISSET($data['controller']->reqlogged) AND $data['controller']->reqlogged AND $data['user']->logged == false) {
            $data->set('arglist', $list.' reqlogged');
            loadView('user_login');
            $content = ob_get_contents();
            ob_end_clean();
            $data->set('content',$content);
        } else if (loadView($view)) {
            $data->set('arglist', $list);
            if ((ISSET($data['controller']->frame) AND $data['controller']->frame == 1) OR (ISSET($_GET['frame']))) {
                $content = ob_get_contents();
                ob_end_clean();
                die($content);
            } else {
                $data->set('content', ob_get_contents());
                ob_end_clean();
            }
        } else {
            $data->set('arglist', $list);
        }
    }

    private function getController(&$file, &$controller, &$action, &$args, &$list) {
        $_SERVER['SCRIPT_URL'] = substr($_SERVER['SCRIPT_URL'], 1);
        $route = (empty($_SERVER['SCRIPT_URL'])) ? 'index' : $_SERVER['SCRIPT_URL'];
        $route = trim($route, '/\\');
        $parts = explode('/', $route);
        $list = implode(" ", $parts);
        $cmd_path = $this->path . 'controllers' . DS;
        $fullpath = $cmd_path;
        $controll = null;
        $controller;
        foreach ($parts as $part) {
            $fullpath = $fullpath . $part;
            $controll = $controll . $part;
            if (is_file($fullpath . '.php') AND ((count($parts) - 1) == 0)) {
                $controller = $controll;
                array_shift($parts);
                break;
            }
            if (is_dir($fullpath)) {
                $fullpath = $fullpath . DS;
                $controll = $controll . '_';
                array_shift($parts);
                continue;
            }
        }
        if (empty($controller)) {
            $controller = 'index';
            $list = 'index';
            $args['err'] = '404';
            header("HTTP/1.0 404 Not Found");
        };
        $file = $cmd_path . $controller . '.php';
        $args['parts'] = $parts;
    }

}

?>