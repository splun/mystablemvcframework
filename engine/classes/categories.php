<?php
class categories{
    private function getList($wherelist, $limit){
        $data = data::init();
        $wlist = array();
        foreach($wherelist AS $key => $value){
            $wlist[] = "`$key`='$value'";
        }
        $where = implode(' AND ',$wlist);
        $where = ' WHERE '.$where;
        $limit = ' LIMIT '.$limit;
        $q = "SELECT * FROM categories$where$limit";
        $list = $data['db']->query($q);
        return $list;
    }
    function getAll(){
        $data = data::init();
        $res = $this->getList(array('lang'=>'en_EN'),20);
        return query_to_array($res);
    }
    function getName($id){
        $res = $this->getList(array('id'=>$id),1);
        $res = mysql_fetch_array($res);
        return $res['name'];
    }
}