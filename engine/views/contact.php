<div class="info">
    <h2 id='head'>Contact information</h2>
    <h2 class="bold">Earthinks</h2>
    <h2>Hydepark 10</h2>
    <h2>Orangerie</h2>
    <h3>3941 zj Domm</h3>
    <h3>The Netherlands</h3>
    <div id="addresses">
        <a class="link" href="mailto:info@earthinks.nl">info@earthinks.nl</a><br>
        <a class="link" href="mailto:admin@eartinks.nl">admin@eartinks.nl</a>
    </div>
</div>
<div class="feedform">
    <h2 id='head'>Feedback</h2>
    <form action="/action/contact" method="POST">
        <div class="data line">
            <label for="mail" style="text-align: right;"><?php lang::str('user', 'Your mail', 'Your e-mail'); ?></label>
            <input name="mail" type="text" class="mail">
            <div class="res err mail"></div>
        </div>
        <div class="data line">
            <label for="question" style="text-align: right;"><?php lang::str('user', 'Your question', 'Your question'); ?></label>
            <textarea name="question" class="mail"></textarea>
            <div class="res err question"></div>
        </div>
        <div id='addit'><?php //lang::str('user', 'captcha', 'captcha');  ?>Robots protection: type the characters you see in the picture below</div>
        <div class="data line">
            <label></label>
            <img src="/kaptcha.php" id="captcha">
            <div class="res err birthday"></div>
        </div>

        <div class="data line">
            <label for="captcha"></label>
            <input name="captcha" type="text" class="captcha">
            <div class="res err captcha"></div>
        </div>
        <div class="data line">
            <label></label>
            <input type="submit" name="Send" value="<?php lang::str('global', 'send', 'Send'); ?>">
        </div>
        <div class="res success"></div>
    </form>
</div>