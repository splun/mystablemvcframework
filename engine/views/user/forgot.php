<div class="forgot">
    <h2 id='head'><?php lang::str('user', 'Forgot password', 'Forgot password'); ?></h2>
    <form action="/action/forgot" method="POST">
        <div class="data line">
            <label for="mail"><?php lang::str('user', 'mail', 'Mail'); ?></label>
            <input name="mail" type="text" class="mail">
            <div class="res err mail"></div>
        </div>
        <div id='addit'><?php lang::str('user', 'captcha', 'captcha'); ?></div>
        <div class="data line">
            <label></label>
            <img src="/kaptcha.php" id="captcha">
        </div>
        
        <div class="data line">
            <label for="captcha"></label>
            <input name="captcha" type="text" class="captcha">
            <div class="res err captcha"></div>
        </div>
        <div class="data line">
            <input type="submit" name="Send" value="<?php lang::str('global', 'send', 'Send'); ?>">
        </div>
        <div class="res success"></div>
    </form>
</div>