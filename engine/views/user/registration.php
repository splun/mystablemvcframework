<div class="left">
    <?php loadView('html_left'); ?>

</div>
<div class="right">

    <form action="/action/registration" method="post" name="registration">
        <div class="data line">
            <label for="name"><?php lang::str('user', 'name', 'Name'); ?></label>
            <input name="name" type="text">

            <div class="res err name"></div>

        </div>
        <div class="data line">
            <label for="surname"><?php lang::str('user', 'surname', 'Surname'); ?></label>
            <input name="surname" type="text">

            <div class="res err surname"></div>

        </div>
        <div class="data line">
            <label for="login"><?php lang::str('user', 'login', 'Login'); ?></label>
            <input name="login" type="text">

            <div class="res err login"></div>

        </div>
        <div class="data line">
            <label for="mail"><?php lang::str('user', 'mail', 'Mail'); ?></label>
            <input name="mail" type="text" class="mail">
            <div class="res err mail"></div>
        </div>
        <div class="data line">
            <label for="password"><?php lang::str('user', 'password', 'Password'); ?></label>
            <input name="password" type="password" class="pass">

            <div class="res err password"></div>

        </div>
        <div class="data line">
            <label for="conf_password"><?php lang::str('user', 'password_repeat', 'Repeat password'); ?></label>
            <input name="conf_password" type="password" class="pass">

            <div class="res err conf_password"></div>

        </div>
        <div class="data line">
            <label for="birthday"><?php lang::str('user', 'birthday', 'Birthday'); ?></label>
            <input name="birthday" type="text">

            <div class="res err birthday"></div>

        </div>
        <div class="data line">
            <input type="submit" name="Send" value="<?php lang::str('global', 'send', 'Send'); ?>">
        </div>
        <div class="res success"></div>
    </form>
    
</div>