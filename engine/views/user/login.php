
<div class="left">
    <form action="/action/login" method="post" name="registration">
        <div class="data line">
            <label></label>
            <h2 id="head"><?php lang::str('user', 'login', 'Login'); ?></h2>
            <div class="res err birthday"></div>
        </div>
        <div class="data line">
            <label for="login"><?php lang::str('user', 'login', 'Login'); ?></label>
            <input name="login" type="text">
            <div class="res err login"></div>
        </div>
        <div class="data line">
            <label for="password"><?php lang::str('user', 'password', 'Password'); ?></label>
            <input name="password" type="password" class="pass">
            <div class="res err password"></div>
        </div>
        <div class="data line">
            <label for="remember"></label>
            <input name="remember" type="checkbox"><?php lang::str('user', 'remember me', 'Remember me'); ?>
        </div>
        <div class="data line">
            <label></label>
            <input type="submit" name="Send" value="<?php lang::str('global', 'send', 'Send'); ?>">
        </div>
        <div class="res success"></div>
        <div class="data line">
            <label></label>
            <a class="link" href="/user/forgot"><?php lang::str('user', 'Forgot password', 'Forgot password'); ?></a>
        </div>
    </form>

</div>

<div class="right">

    <form action="/action/registration" method="post" name="registration">
        <div class="data line">
            <label></label>
            <h2 id="head"><?php lang::str('user', 'create account', 'Create account'); ?></h2>
        </div>

        <div class="data line">
            <label for="mail"><?php lang::str('user', 'mail', 'Mail'); ?></label>
            <input name="mail" type="text" class="mail">
            <div class="res err mail"></div>
        </div>
        <div class="data line">
            <label for="login"><?php lang::str('user', 'login', 'Login'); ?></label>
            <input name="login" type="text">
            <div class="res err login"></div>
        </div>
        <div class="data line">
            <label></label>
            <a class="link" href="mailto:info@earthinks.nl">info@earthinks.nl</a>
        </div>
        <div class="data line">
            <label></label>
            <a class="link" href="mailto:admin@eartinks.nl">admin@eartinks.nl</a>
        </div>
        <div class="data line">
            <label for="password"><?php lang::str('user', 'password', 'Password'); ?></label>
            <input name="password" type="password" id="pass" class="pass" onkeyup="runPassword(this.value, 'pass');">
            <div id="pass_text"></div>
            <div class="res err password"></div>

        </div>
        <div class="data line">
            <label for="conf_password"><?php lang::str('user', 'password_repeat', 'Repeat password'); ?></label>
            <input name="conf_password" type="password" class="pass">
            <div class="res err conf_password"></div>
        </div>
        <div class="data line">
            <label for="remember"></label>
            <input name="remember" type="checkbox"><?php lang::str('user', 'remember me', 'Remember me'); ?>
        </div>
        <div style="width:100%; height:30px;"></div>
        <div class="data line">
            <label></label>
            <h3 id="head">Start using earthinks</h3>
        </div>

        <div class="data line">
            <label for="name"><?php lang::str('user', 'name', 'Name'); ?></label>
            <input name="name" type="text">

            <div class="res err name"></div>

        </div>
        <div class="data line">
            <label for="surname"><?php lang::str('user', 'surname', 'Surname'); ?></label>
            <input name="surname" type="text">

            <div class="res err surname"></div>

        </div>
        <div class="data line">
            <label for="birthday"><?php lang::str('user', 'birthday', 'Birthday'); ?></label>
            <input name="birthday" type="text" id="datepicker">
            <div class="res err birthday"></div>
        </div>
        <div class="data line">
            <label for="country"><?php lang::str('user', 'country', 'Country'); ?></label>
            <select name="country">
                <?php
                $countries = new countries();
                $list = $countries->getAll();
                foreach ($list AS $key => $value) {
                    echo '<option value="' . $value['id'] . '">' . $value['country_name_en'] . '</option>';
                }
                ?>
            </select>
            <div class="res err country"></div>
        </div>
        <div style="width:100%; height:30px;"></div>
        <div id='addit'><?php lang::str('user', 'robots protection', 'Robots protection: type the characters you see in the picture below'); ?></div>
        <div class="data line">
            <label></label>
            <img src="/kaptcha.php" id="captcha">
        </div>

        <div class="data line">
            <label for="captcha"></label>
            <input name="captcha" type="text" class="captcha">
            <div class="res err captcha"></div>
        </div>
        <div class="data line">
            <label for="terms"><?php lang::str('global', 'terms of use', 'Terms of use'); ?></label>
            <input name="terms" type="checkbox"><?php
                lang::str('user', 'i accept', 'I accept');
                echo "<a class='link' href='/page/view?p=4&frame=true' id='terms'>";
                lang::str('global', 'terms of use', 'Terms of use');
                echo'</a>';
                ?>
        </div>
        <div class="data line">
            <label></label>
            <input type="submit" name="Send" value="<?php lang::str('global', 'create account', 'Create account'); ?>">
        </div>
        <div class="data line">
            <label></label>
            or <a class="link" href="/">return to Earthinks home</a>
        </div>
        <div class="res success"></div>
    </form>
</div>
<script>
    $( "#datepicker" ).datepicker({
        monthNames: [ "Januar", "Februar", "Marts", "April", "Maj", "Juni", "Juli", "August", "September", "Oktober", "November", "December" ],
        changeYear: true,
        changeMonth: true,
        dateFormat: "mm/dd/yy",
        yearRange: '1910:2010'
    });
    $(document).ready(function(){
        $("a#terms").click(function(){
            
            $.ajax({
                type: "GET",
                cache: false,
                url: "/page/view?p=4&frame=true",
                data: $(this).serializeArray(),
                success: function(data) {
                    $.fancybox(data);
                }
            });

            return false;
            
        })
       
    })
    
</script>