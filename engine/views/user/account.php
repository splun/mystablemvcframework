<?php
if ($data['controller']->info['avatar'] == '') {
    $data['controller']->info['avatar'] = 'style/images/avatar.jpg';
}
?>
<h2 id='head'><?php echo $data['controller']->info['name']; ?> <?php echo $data['controller']->info['surname']; ?>`s Personal page</h2>
<div id="maininfo">
    <div id="avatar">
        <img src="/<?php echo $data['controller']->info['avatar']; ?>" id="avatar">
    </div>
    <div id="info">
        <div id='uname'><?php echo $data['controller']->info['name']; ?> <?php echo $data['controller']->info['surname']; ?></div>
        <div id='bdate'><?php echo dateloc::date($data['controller']->info['birth']); ?></div>
    </div>
</div>
<div id="adinfo">
    <?php echo $data['controller']->info['addinfo']; ?>
</div>
<?php
$photos = '';
$w = 0;
foreach ($data['controller']->photos AS $photo) {
    $w += 160;
    $photos = "$photos
        <div id='photo'>
            <a href='/album/photo?id={$photo['id']}'>
                <img src='{$photo['serv']}{$photo['thumb']}'>
            </a>
        </div>";
}
if($w > 0){ ?>
<div id='userphotos'>
    <div class="container"  style='width: <?php echo $w; ?>px;'>
        <?php echo $photos; ?>
    </div>
</div>
<?php } ?>
<div id="messend">
    <form action="/action/message" method="post" name="messend">
        <div class="data line">
            <h2 id="head"><?php lang::str('global', 'Send message', 'Send message'); ?></h2>
        </div>
        <div class="data line">
            <textarea name="text" style="width:700px; height: 90px; resize: none;"></textarea>
            <div class="res err message"></div>
        </div>
        <input type="hidden" name="to" value="<?php echo $data['controller']->id; ?>">
        <div class="data line">
            <input type="submit" name="Send" value="<?php lang::str('global', 'send', 'Send'); ?>">
        </div>
        <div class="res success"></div>
    </form>
</div>
<script>
    $(document).ready(function() {
        $("#userphotos").mCustomScrollbar({
            set_height: '170px',
            horizontalScroll: true,
            scrollButtons: {
                enable: true
            },
            advanced: {
                autoExpandHorizontalScroll: true
            }
        });
    });
</script>