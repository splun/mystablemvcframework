<div id="meslist">
    <?php
    $data = data::init();
    if (count($data['controller']->messages)) {
        foreach ($data['controller']->messages as $key => $value) {
            $read = '';
            if($value['read'] == 0){
                $read = 'class="notread"';
            }
            ?>
            <div id="message" <?php echo $read; ?>>
                <div id="avatar">
                    <img src="/<?php
                    if (strlen($value['avatar']) < 5) {
                        $value['avatar'] = 'style/images/avatar.jpg';
                    }
                    echo $value['avatar'];
                    ?>">
                </div>
                <div id="name"><a href="/user/account?id=<?php
                    echo $value['from'] . '">from';
                    echo ': ' . $value['name'] . ' ' . $value['surname'];
                    ?></a></div>
                                  <div id="time"><?php echo dateloc::datetime($value['time']); ?></div>
                <div id="text"><?php echo $value['message']; ?></div>
            </div>
            <?php
        }
    }
    ?>
</div>
<form action="/action/message" method="post" name="message" id="message" style="width: 645px; margin: auto;">
    <div class="data line">
        <textarea name="text" style="margin: 0px; width: 100%; height: 60px;"></textarea>
        <div class="res err message"></div>
    </div>
    <input type="hidden" name="to" value="<?php echo $_GET['with']; ?>">
    <div class="data line">
        <input type="submit" name="Send" value="<?php lang::str('global', 'send', 'Send'); ?>">
    </div>
    <div class="res success" style="text-align: center;"></div>
</form>
<div id="bot"></div>
<script>
    $(document).ready(function() {
        $('form#message').ajaxForm({
            dataType: 'json',
            target: $(this).find('form'),
            beforeSubmit: function(data, $form) {
                $('.res', $form).text('');
            },
            success: function(data, status, xhr, form) {
                var parent = 0;
                $.each(data, function(key, val) {
                    $('.res.' + key, form).text(val).show();
                    if (key == 'text') {
                        $('#meslist').append(val);
                    }
                });
            }
        });
    })
</script>