<?php
if ($data['controller']->info['avatar'] != '') {
    $avatar = $data['controller']->info['avatar'];
} else {
    $avatar = 'style/images/avatar.jpg';
}
?>
<script type="text/javascript">
    $(document).ready(function() {
        $('#avatar').change(function() {
            $('#avatar').each(function() {
                var name = this.value;
                reWin = /.*\\(.*)/;
                var fileTitle = name.replace(reWin, "$1");
                reUnix = /.*\/(.*)/;
                fileTitle = fileTitle.replace(reUnix, "$1");
                $('#name').html(fileTitle);
            });
        });
    });
</script>

<div id="tabs">
    <ul>
        <li><a href="#settings">Account settings</a></li>
        <li><a href="#photos-conf">Active photos</a></li>
        <li><a href="#photos-notconf">Not approved</a></li>
        <li><a href="#messages">Messages (<?php echo $data['controller']->mesnew ?> new)</a></li>
        <li><a href="#earthspots">Earthspots</a></li>
    </ul>
    <div id="settings">
        <form method="POST" action="/action/accsettings" class="accsettings">
            <div class="data line">
                <label></label>
                <h3 id="head"><?php lang::str('user', 'Personal', 'Personal'); ?></h3>
            </div>
            <div id="userinfo">
                <div class="data line">
                    <label for="name"><?php lang::str('user', 'name', 'name'); ?></label>
                    <input name="name" type="text" class="name" value="<?php echo $data['controller']->info['name']; ?>">
                    <div class="res err name"></div>
                </div>
                <div class="data line">
                    <label for="surname"><?php lang::str('user', 'surname', 'surname'); ?></label>
                    <input name="surname" type="text" class="surname" value="<?php echo $data['controller']->info['surname']; ?>">
                    <div class="res err surname"></div>
                </div>
                <div class="data line">
                    <label for="birth"><?php lang::str('user', 'birthday', 'birthday'); ?></label>
                    <input name="birth" type="text" class="birth" id="datepicker"  value="<?php echo $data['controller']->info['birth']; ?>">
                    <div class="res err birth"></div>
                </div>
                <div class="data line">
                    <label for="addinfo"><?php lang::str('user', 'other information', 'Other information'); ?></label>
                    <textarea name="addinfo"><?php echo $data['controller']->info['addinfo']; ?></textarea>
                    <div class="res err addinfo"></div>
                </div>
            </div>
            <div id="avainf">
                <div class="data line">
                    <label></label>
                    <div id="curavatar">
                        <img src="/<?php echo $avatar; ?>" class="avatar">
                        <a id="avadel" class="link">Remove avatar</a>
                        <div class="progress">
                            <div class="bar"></div >
                            <div class="percent">0%</div >
                        </div>
                    </div>
                </div>
                <div class="data line">
                    <label for="avatar">Avatar</label>
                    <div id="avatarfl">
                        <div id="name"></div>
                        <div id="avasel"><input type="file" name="avatar" id="avatar">...</div>

                    </div>
                    <div class="res err avatar"></div>
                </div>
            </div>
            <div class="data line divider">
                <label></label>
                <h3 id="head"><?php lang::str('user', 'Change password', 'Change password'); ?></h3>
            </div>
            <div class="data line">
                <label for="pass_change"><?php lang::str('user', 'Change password', 'Change password'); ?></label>
                <input name="pass_change" type="checkbox">
                <div class="res err pass_change"></div>
            </div>
            <div class="data line">
                <label for="password"><?php lang::str('user', 'password', 'Password'); ?></label>
                <input name="password" type="password" id="pass" class="pass" onkeyup="runPassword(this.value, 'pass');">
                <div id="pass_text"></div>
                <div class="res err password"></div>
            </div>

            <div class="data line">
                <label for="conf_password"><?php lang::str('user', 'password_repeat', 'Repeat password'); ?></label>
                <input name="conf_password" type="password" class="pass">
                <div class="res err conf_password"></div>
            </div>
            <div class="data line">
                <label></label>
                <h3 id="head"><?php lang::str('user', 'captcha', 'captcha'); ?></h3>
            </div>
            <div class="data line">
                <label></label>
                <img src="/kaptcha.php" id="captcha">
                <div class="res err birthday"></div>
            </div>

            <div class="data line">
                <label for="captcha"></label>
                <input name="captcha" type="text" class="captcha">
                <div class="res err captcha"></div>
            </div>
            <div class="data line">
                <h3 id="head">Please type your current password</h3>
            </div> 
            <div class="data line">
                <label for="curpassword"><?php lang::str('user', 'password', 'Password'); ?></label>
                <input name="curpassword" type="password" class="pass">
                <div class="res err curpassword"></div>
            </div>
            <div class="data line">
                <input type="submit" name="Send" value="<?php lang::str('global', 'send', 'Send'); ?>">
            </div>
            <div class="res success"></div>
        </form>
    </div>
    <div id="photos-conf">
        <?php
        if (count($data['controller']->conf)) {
            foreach ($data['controller']->conf as $key => $value) {
                ?>
                <form class="frm">
                    <a href="/user/account/photo?id=<?php echo $value['id']; ?>"><img src='<?php echo $value['serv'] . $value['thumb']; ?>'></a>
                    <div id = 'subdata'>
                        <div class="data line">
                            <?php lang::str('photo', 'photo name', 'Photo name'); ?>
                            <?php echo $value['name']; ?>
                            <div class="res err name"></div>
                        </div>
                        <div class="data line">
                            <?php lang::str('photo', 'location', 'Location'); ?><br>
                            X: <?php echo $value['x_dest']; ?><br>
                            Y: <?php echo $value['y_dest']; ?>
                            <div class="res err x_dest"></div><div class="res err y_dest"></div>
                        </div>
                        <div class="data line">
                            <?php lang::str('photo', 'description', 'Description'); ?><br>
                            <?php echo $value['description']; ?>
                            <div class="res err x_dest"></div><div class="res err y_dest"></div>
                        </div>
                    </div>
                </form>
                <?php
            }
        } else {
            echo 'No new photos';
        }
        ?>
    </div>
    <div id="photos-notconf">
        <?php
        if (count($data['controller']->notconf)) {
            foreach ($data['controller']->notconf as $key => $value) {
                ?>
                <form action='/action/photoedit' class="frm" method="POST">
                    <a href="/user/account/photo?id=<?php echo $value['id']; ?>"><img src='<?php echo $value['serv'] . $value['thumb']; ?>'></a>
                    <div id = 'subdata'>
                        <div class="data line">
                            <?php lang::str('photo', 'photo name', 'Photo name'); ?>
                            <input name="name" type="text" value="<?php echo $value['name']; ?>">
                            <div class="res err name"></div>
                        </div>
                        <div class="data line">
                            <?php lang::str('photo', 'location', 'Location'); ?><br>
                            X: <input name="x_dest" type="text" value="<?php echo $value['x_dest']; ?>"><br>
                            Y: <input name="y_dest" type="text" value="<?php echo $value['y_dest']; ?>">
                            <div class="res err x_dest"></div><div class="res err y_dest"></div>
                        </div>
                        <div class="data line">
                            <?php lang::str('photo', 'description', 'Description'); ?><br>
                            <textarea name ="description"><?php echo $value['description']; ?></textarea>
                            <div class="res err x_dest"></div><div class="res err y_dest"></div>
                        </div>
                        <select name="act">
                            <option value="1">Update</option>
                            <option value="0">Delete</option>
                        </select>
                        <input type="submit">
                    </div>
                    <input type="hidden" value="<?php echo $value['id']; ?>" name="photo">
                    <div class="res success"></div>
                    <div class="res fail"></div>
                </form>
                <?php
            }
        } else {
            echo 'No new photos';
        }
        ?>
    </div>
    <div id="messages">
        <div id="lastmes">
            <?php
            if (count($data['controller']->messages)) {
                foreach ($data['controller']->messages as $key => $value) {
                    $read = '';
                    if (ISSET($value['read']) AND $value['read'] == 0) {
                        $read = 'class="notread"';
                    }
                    ?>
                    <a href="/user/account/messages?with=<?php echo $key ?>" id="mespane">
                        <div id="message"  <?php echo $read; ?>>
                            <div id="avatar">
                                <img src="/<?php
                                if (strlen($value['avatar']) < 5) {
                                    $value['avatar'] = 'style/images/avatar.jpg';
                                }
                                echo $value['avatar'];
                                ?>">
                            </div>
                            <div id="name"><a href="/user/account?id=<?php
                                if (ISSET($value['to'])) {
                                    echo $value['to'] . '">to';
                                } else {
                                    echo $value['from'] . '">from';
                                } echo ': ' . $value['name'] . ' ' . $value['surname'];
                                ?></a></div>
                                              <div id="time"><?php echo dateloc::datetime($value['time']); ?></div>
                            <div id="text"><?php echo $value['message']; ?></div>
                        </div>
                    </a>
                    <?php
                }
            }
            ?>           
        </div>
        <div id="mcs_container">
            <div class="customScrollBox">
                <div class="container">
                    <div id="meslst"></div>
                    <div id="botscr"></div>
                </div>
                <div class="dragger_container">
                    <div class="dragger"></div>
                </div>
            </div>
        </div>
    </div>
    <div id="earthspots">
        <?php foreach ($data['controller']->earthspots AS $eartspot) { ?>
            <div id='spot'>
                <div id ='image'>
                    <img src='<?php echo $eartspot['serv'] . $eartspot['file']; ?>'>
                </div>
                <div id='info'>
                    <div id='name'>
                        <?php echo $eartspot['name']; ?>
                    </div>
                    <div id='shortdesc'>
                        <?php echo cutString($eartspot['shortdesc'], 100); ?>
                    </div>
                    <div id='description'>
                        <?php echo cutString($eartspot['description'], 250); ?>
                    </div>
                    <div id='links'>
                        <a href='<?php echo "/album/thinkspot?id={$eartspot['avatar']}&group={$eartspot['id']}"; ?>' class='red'>View</a> / <a href='<?php echo "/album/thinkcreate?id={$eartspot['id']}"; ?>' class='red'>Edit</a>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
</div>

<script>
    $(document).ready(function() {
        $("#tabs").tabs();
        $("#mcs_container").mCustomScrollbar({
            verticalScroll: true,
            set_height: '500px',
            autoDraggerLength: true,
            contentTouchScroll: true,
            scrollButtons: {
                enable: true
            },
            advanced: {
                updateOnContentResize: true,
                autoScrollOnFocus: true
            }
        });
        $("#datepicker").datepicker({
            monthNames: ["Januar", "Februar", "Marts", "April", "Maj", "Juni", "Juli", "August", "September", "Oktober", "November", "December"],
            changeYear: true,
            changeMonth: true,
            dateFormat: "dd-mm-yy"
        });
        $("#curavatar #avadel").click(function() {
            $.ajax({url: '/action/user/avadel'}).success(function() {
                $("#curavatar").html('Deleted')
            });
        });
        $("a#mespane").click(function() {
            var href = $(this).attr('href');
            href = href + '&frame';
            $.ajax({
                url: href,
                async: "false"
            }).success(function(data) {
                $("#meslst").html(data);
                $("#mcs_container").mCustomScrollbar("update");
            });
            return false;
        })
    });
</script>