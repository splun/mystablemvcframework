<?php
$approved = $data['controller']->approved;
?>
<img src="<?php echo $data['controller']->serv . $data['controller']->thumb; ?>" id="alb">
<form method="POST" action='/action/photo/edit'>
    <h2 id="head">
        <?php echo $data['controller']->name; ?>
    </h2>
    <div class="data line">
        <label for="name"><?php lang::str('user', 'name', 'Name'); ?></label>
        <?php
        if (!$approved) {
            echo '<input name="name" type="text" value="' . $data['controller']->name . '">';
        } else {
            echo $data['controller']->name;
        }
        ?>
        <div class="res err name"></div>
    </div>
    <div class="data line">
        <label for="description"><?php lang::str('photo', 'description', 'description'); ?></label>
        <?php
        if (!$approved) {
            echo '<textarea name="description">' . $data['controller']->description . '</textarea>';
        } else {
            echo $data['controller']->description;
        }
        ?>
        <div class="res err description"></div>
    </div>
    <div class="data line">
        <label for="categories"><?php lang::str('photo', 'category', 'Category'); ?></label>
        <?php if (!$approved) { ?>
            <select name="categories">
                <?
                $cat = new categories();
                $cats = $cat->getAll();
                foreach ($cats AS $key => $value) {
                    $active = '';
                    if ($value['id'] == $data['controller']->category) {
                        $active = ' selected="selected"';
                    }
                    echo "<option value='{$value['id']}'$active>{$value['name']}</option>";
                }
                ?>
            </select>
            <?php
        } else {
            $cat = new categories();
            echo $cat->getName($data['controller']->category);
        }
        ?>
        <div class="res err categories"></div>
    </div>
    <div class="data line">
        <label for="acctype">Type of access</label>
        <select name="acctype" id="acctype" onchange="usel()">
            <option value="0" id="all">For all</option>
            <option value=<?php
        echo '"' . $data['controller']->id . '"';
        if ($data['controller']->rights) {
            echo ' selected';
        }
        ?>>Private</option>
        </select>
    </div>
    <div class="data line" id="private">
        <label for="adduser"><?php lang::str('photo', 'add user', 'Add user'); ?></label>
        <input name="adduser" type="text" id="adduser">
        <div class="res err adduser"></div>
        <ul id="userlist"></ul>
        <ul id="selectlist">

            <?php
            if (count($data['controller']->allowed)) {
                foreach ($data['controller']->allowed as $key => $value) {
                    if (!strlen($value['avatar'])) {
                        $value['avatar'] = 'style/images/avatar.jpg';
                    }
                    ?>
                    <div id="user" class="<?php echo $value['uid']; ?>">
                        <input name="usr[]" type="checkbox" value="<?php echo $value['uid']; ?>" id="usrlist" checked>
                    <div id="names"><?php echo $value['name'] . ' ' . $value['surname'] . '</div><img src="/' . $value['avatar'] . '"> '; ?>
                    </div>
                    <?php
                }
            }
            ?>
        </ul>
    </div>
    <input type="hidden" name="pid" value="<?php echo $data['controller']->id ?>">
        <div class="res success"></div>
    <div class="data line">
        <label></label>
        <input type="submit" name="Send" value="<?php lang::str('global', 'send', 'Send'); ?>">
    </div>
</form>
<script>
    function log( message ) {
        console.log(message);
    }
    $( "input#adduser" ).keyup(function(){
        $.getJSON('/user/list?term='+encodeURI($('input#adduser').val()), function(data) {
            var items = [];
            $.each(data, function(key, val) {
                if(val.avatar.length < 1){
                    val.avatar = 'style/images/avatar.jpg';
                }
                var checked = '';
                if($('#user.'+val.id).is('.'+val.id)){
                    checked = ' checked';
                }
                items.push('<li><input type="checkbox" '+checked+' value="'+val.id+'" id="usrlist" avatar="' + val.avatar + '" uname="' + val.name + '" usurname="' + val.surname + '" /><img src="/' + val.avatar + '" class="ava"><div id="names">' + val.name + ' ' + val.surname + " " + '</div></li>');
            });
            $('ul#userlist').html(items.join(''));
        });
        
    });
    function usel(){
        $('#private').toggle();
    }
    $(document).ready(function(){
        if($('select#acctype').val() == 0){
            $('#private').toggle();
        }
    });
    $(document).on('click','input#usrlist',function(){
        var id = $(this).val();
        if($('#user.'+id).is('.'+id)){
            $('ul#selectlist #user.'+id).remove();
        } else {
            var name = $(this).attr('uname');
            var surname = $(this).attr('usurname');
            var ava = $(this).attr('avatar');
            $('ul#selectlist').append('<div id="user" class="'+id+'"><input name="usr[]" type="checkbox" value="'+id+'" id="usrlist" checked><div id="names">' + name + ' ' +  ' ' + surname + '</div><img src="/' + ava + '" class="ava"></div>');
            console.log(name + ' ' +  ' ' + surname +  ' ' + id +  ' ' + ava);
            console.log('checked');
        }
    })
</script>