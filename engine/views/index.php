<?php
$args = $data->get('args');
if(ISSET ($args['err']) AND $args['err'] == '404'){ ?>
<div id="error">
    Page not found
</div>
<?php } ?>
<div class="left_pane">
    <div id="last_photo">
        <?php
        $res = $data['db']->query("SELECT photos.*, users.name AS uname, users.surname AS usurname FROM photos INNER JOIN users ON photos.owner = users.id WHERE photos.status = '1' ORDER BY photos.id DESC LIMIT 1");
        $res = mysql_fetch_array($res);
        echo '<a id="photoimg" href="/album/photo?id=' . $res['id'] . '"><img src = "' . $res['serv'] . $res['file'] . '" id="photo"></a>';
        echo '<div class="info">';
        echo '<div id="name">' . $res['name'] . '</div>';
        echo '<div id="date">' . dateloc::date($res['added']) . '</div>';
        echo '<div id="author">Author ' . $res['usurname'] . ' ' . $res['uname'] . '</div>';
        echo '<div id="description">' . $res['description'] . '</div></div>';
        ?>
    </div>
</div>
<div id="map_canvas"></div>
<script>
    var map;
    function initialize() {
        var mapOptions = {
            zoom: 10,
            minZoom: 3,
            center: new google.maps.LatLng(<?php echo $res['x_dest'] ?>, <?php echo $res['y_dest'] ?>),
            disableDefaultUI: true,
            mapTypeId: google.maps.MapTypeId.TERRAIN  
        };
        var map = new google.maps.Map(document.getElementById('map_canvas'),mapOptions);
        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(<?php echo $res['x_dest'] ?>, <?php echo $res['y_dest'] ?>),
            map: map
        });
    }
    google.maps.event.addDomListener(window, 'load', initialize);
</script>