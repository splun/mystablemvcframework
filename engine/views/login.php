<div id='addit'>This action requiers to be logged in</div>
<h2 id="head">Login</h2>
<form action="/action/login" method="post" name="registration">
    <div class="data line">
        <label for="login"><?php lang::str('user', 'login', 'Login'); ?></label>
        <input name="login" type="text">
        <div class="res err login"></div>
    </div>
    <div class="data line">
        <label for="password"><?php lang::str('user', 'password', 'Password'); ?></label>
        <input name="password" type="password" class="pass">
        <div class="res err password"></div>
    </div>
    <div class="data line">
        <input type="submit" name="Send" value="<?php lang::str('global', 'send', 'Send'); ?>">
    </div>
    <div class="res success"></div>
</form>
<a href="/user/forgot"><?php lang::str('user', 'Forgot password', 'Forgot password'); ?></a>
<a href="/user/login"><?php lang::str('global', 'registration', 'registration'); ?></a>