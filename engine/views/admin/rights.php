<div class="userlist">
    <form id="list" method="POST">
        <div class="data line">
            <label for="name"></label>
            <input name="name" type="text" id="name">
        </div>
        <div class="res success"></div>
    </form>
    <ul id="userlist">

    </ul>
</div>
<form id="rights" method="POST" action="/action/rights">
    <input type="hidden" value="1" id="user" name="user">
    <div class="data line">
        <h2 id="head"></h2>
        <div class="res err birthday"></div>
    </div>
    <div class="data line">
        <label for="access"><?php lang::str('admin', 'access type', 'Access type'); ?></label>
        <select name="access" id="access">
            <option value="0">None</option>
            <option value="1">Admin</option>
            <option value="2">Moderator</option>
            <option value="3">Censor</option>
        </select>
        <div class="res err access"></div>
    </div>
    <div class="data line categories">
        <?php
        $cat = new categories();
        $cats = $cat->getAll();
        ?>
        <label for="categories"><?php lang::str('user', 'password', 'Password'); ?></label>
        <div id="block">
            <?php foreach ($cats AS $key => $value) { ?>
                <input type="checkbox" name="cat[]" value="<?php echo $value['id'] ?>" id="<?php echo $value['id'] ?>"><?php echo $value['name'] ?><br>
            <?php } ?>
        </div>
        <div class="res err categories"></div>
    </div>
    <div class="data line">
        <label></label>
        <input type="submit" name="Send" value="<?php lang::str('global', 'send', 'Send'); ?>">
    </div>
    <div class="res success"></div>
</form>
<script>
    $("#block input").attr('disabled','disabled');
    $("input#name").keyup(function() {
        $.getJSON('/user/list?term=' + encodeURI($('input#name').val()), function(data) {
            var items = [];
            $.each(data, function(key, val) {
                if (val.avatar.length < 1) {
                    val.avatar = 'style/images/avatar.jpg';
                }
                var checked = '';
                if ($('#user.' + val.id).is('.' + val.id)) {
                    checked = ' checked';
                }
                items.push('<li id="user" class="' + val.id + '" unm="' + val.name + ' ' + val.surname + '"><img src="/' + val.avatar + '" class="ava"><div id="names">' + val.name + ' ' + val.surname + " " + '</div></li>');
            });
            $('ul#userlist').html(items.join(''));
        });
    });
    $("li#user").live("click", function() {
        $("select#access :nth-child(1)").attr("selected", "selected");
        id = $(this).attr('class');
        usrname = $(this).attr('unm');
        $.getJSON('/user/rights?id=' + id, function(data) {
            $.each(data, function(key, val) {
                val.type = parseInt(val.type) + 1;
                $("select#access :nth-child("+val.type+")").attr("selected", "selected");
                $("#block input#"+val.num).attr("checked", "checked");
            });
        });
        $("form#rights input#user").val(id);
        $("form#rights h2#head").html(usrname);
    });
    $("select#access").change(function(){
        val = $("select#access option:selected").val();
        console.log("CLICK! "+val);
        if(val == 1){
            $("#block input").attr('disabled','disabled');
        } else if(val == 0){
            $("#block input").attr('disabled','disabled');
        } else {
            $("#block input").removeAttr('disabled');
        }
    });
</script>