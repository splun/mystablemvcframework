<?php
$data = data::init();
if (count($data['controller']->list)) {
    foreach ($data['controller']->list as $key => $value) {
        ?>
        <form action='/action/approve' class="admin images" method="POST">
            <img src='<?php echo $value['serv'] . $value['thumb']; ?>'>
            <div id = 'subdata'>
                <div class="data line">
                    <?php lang::str('photo', 'photo name', 'Photo name'); ?>
                    <input name="name" type="text" value="<?php echo $value['name']; ?>">
                    <div class="res err name"></div>
                </div>



                <div class="data line">
                    <?php lang::str('photo', 'location', 'Location'); ?><br>
                    X: <input name="x_dest" type="text" value="<?php echo $value['x_dest']; ?>">
                    Y: <input name="y_dest" type="text" value="<?php echo $value['y_dest']; ?>">
                    <div class="res err x_dest"></div><div class="res err y_dest"></div>
                </div>
                <div class="data line">
                    <?php lang::str('photo', 'description', 'Description'); ?><br>
                    <textarea name ="description"><?php echo $value['description']; ?></textarea>
                    <div class="res err x_dest"></div><div class="res err y_dest"></div>
                </div>
                <select name="act">
                    <option value="1">Approve</option>
                    <option value="0">Delete</option>
                    <option value="2">Recreate</option>
                </select>
                <input type="submit">
            </div>
            <input type="hidden" value="<?php echo $value['id']; ?>" name="photo">
            <div class="res success"></div>
            <div class="res fail"></div>
        </form>
        <?php
    }
} else {
    echo 'No new photos';
}
?>