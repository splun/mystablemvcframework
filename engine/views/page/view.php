<?php
$data = data::init();
//<h2 class="head"><?php echo $data['controller']->name;</h2>
?>
<div class="text-content">
    <?php echo $data['controller']->text; ?>
</div>
    
<?php
if (count($data['controller']->langs)) {
    foreach ($data['controller']->langs as $key => $value) {
        ?>
        <div class="lang">
            <div id="text">
                <a href="/page/view?p=<?php echo $data['controller']->id; ?>&lang=<?php echo $value['lang']; ?>"><?php echo $value['lang']; ?></a>
            </div>
        </div>
        <?php
    }
    ?>

    <?php
}
?> 