<html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <link rel="stylesheet" type="text/css" href="/style/css/custom.php" />
        <link rel="stylesheet" type="text/css" href="/style/css/south-street/jquery-ui-1.9.2.custom.css" />
        <link rel="stylesheet" type="text/css" href="/style/css/jquery.mCustomScrollbar.css">
        <link rel="stylesheet" type="text/css" href="/style/css/jquery.fancybox.css?v=2.1.2" media="screen" />
        <link rel="stylesheet" type="text/css" href="/style/css/jquery.fancybox-buttons.css?v=1.0.5" />
        <link rel="stylesheet" type="text/css" href="/style/css/jquery.fancybox-thumbs.css?v=1.0.7" />
        <script type="text/javascript" src="/style/js/jquery-1.8.3.js"></script>
        <script type="text/javascript" src="/style/js/jquery.form.js"></script>
        <script type="text/javascript" src="/style/js/jquery.easing.1.3.js"></script>
        <script type="text/javascript" src="/style/js/jquery-ui-1.9.2.custom.js"></script>
        <script type="text/javascript" src="/style/js/jquery.mousewheel.min.js" ></script>
        <script type="text/javascript" src="/style/js/jquery.fancybox.js?v=2.1.3"></script>
        <script type="text/javascript" src="/style/js/jquery.fancybox-buttons.js?v=1.0.5"></script>
        <script type="text/javascript" src="/style/js/jquery.fancybox-thumbs.js?v=1.0.7"></script>
        <script type="text/javascript" src="/style/js/jquery.fancybox-media.js?v=1.0.5"></script>
        <script type="text/javascript" src="/style/js/jquery.scrollTo-min.js"></script>
        <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
        <script type="text/javascript" src="http://google-maps-utility-library-v3.googlecode.com/svn/trunk/markerclusterer/src/markerclusterer.js"></script>
        <script type="text/javascript" src="/style/js/jquery.mCustomScrollbar.js" ></script>
        <script type="text/javascript" src="/style/js/custom.js"></script>
        <?php getHead(); ?>

        <script>
            
            $(document).ready(function(){
                $('.footer #carousel').mCustomScrollbar({
                    mouseWheel:false,
                    horizontalScroll:true,
                    scrollButtons:{
                        enable:true
                    },
                    advanced:{
                        autoExpandHorizontalScroll:true
                    }
                });
                $('#photosline').mCustomScrollbar({
                    horizontalScroll:true,
                    scrollButtons:{
                        enable:true
                    },
                    advanced:{
                        autoExpandHorizontalScroll:true
                    }
                });
                $('a.popup').fancybox({
                    'width':'75%',
                    'height':'75%',
                    'autoScale': false,
                    'transitionIn': 'none',
                    'transitionOut': 'none',
                    'type': 'ajax'
                });
                var form;
                $('form').ajaxForm({
                    dataType:  'json',
                    target: $(this).find('form'),
                    beforeSubmit: function(data,$form){
                        $('.res',$form).text('');
                    },
                    uploadProgress: function(event, position, total, percentComplete) {
                        var bar = $('.bar');
                        var percent = $('.percent');
                        var status = $('#status');
                        var percentVal = percentComplete + '%';
                        bar.width(percentVal)
                        percent.html(percentVal);
                    },
                    success: function(data,status,xhr,form){
                        $.each(data,function(key, val){
                            $('.res.'+key, form).html(val).show();
                            if(key == 'reload' ){
                                setTimeout('window.location.href = "'+val+'"', 1000);
                            }
                        });
                        if($('img', form).is("#captcha")){
                            var now = new Date();
                            res = '/kaptcha.php?t=' +  now.getTime();
                            $("img#captcha").attr({ src : res});
                            $("input.captcha").attr({value:''});
                        }
                    }
                });
            })
        </script>
    </head>
    <body class="<?php echo getList(); ?>">
    <div id="bg" class="top"></div>
        <div class="wrapper">
            <div class="header">
                <?php getHeader(); ?>

            </div>
            <div class="content">
                <?php getContent(); ?>

            </div>

        </div>
        <div class="footer">
            <?php getFooter(); ?>

        </div>
    </body>
</html>
