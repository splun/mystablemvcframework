<div class="left">

    <div id="logo">
        <a href="/"><img src="/style/images/logo.png"></a>
        <div class="slogan">Create value by sharing your reality</div>
    </div>
    <div class="left_pane">
        <div id="last_photo">
            <?php
            $data = data::init();
            $res = $data['db']->query("SELECT photos.*, users.name AS uname, users.surname AS usurname FROM photos INNER JOIN users ON photos.owner = users.id WHERE photos.status = '1' ORDER BY photos.id DESC LIMIT 1");
            $res = mysql_fetch_array($res);
            echo '<a id="photoimg" href="/album/photo?id='.$res['id'].'"><img src = "' . $res['serv'] . $res['file'] . '" id="photo"></a>';
            echo '<div class="info">';
            echo '<div id="name">' . $res['name'] . '</div>';
            echo '<div id="date">' . dateloc::date($res['added']). '</div>';
            echo '<div id="author">Author ' . $res['usurname'] . ' ' . $res['uname'] . '</div>';
            echo '<div id="description">' . $res['description'] . '</div></div>';
            ?>
        </div>
    </div>
</div>
<?php //img src="/style/images/divider.png" class="divider"> ?>