<?php
$data = data::init();
$mesnew = query_to_array($data['db']->query("SELECT COUNT(*) as cnt FROM messages WHERE `to` = '{$data['user']->id}' AND `read` = '0'"));
$mes = $mesnew[0]['cnt'];
if ($mes > 0)
    $mes = " <a href='/user/account/panel#messages' style='font-size: 12px;'>Messages: $mes</a>";
else
    $mes = '';
?>
<div id="top">
    <ul id="menu">
        <li><a href="/"><?php lang::str('global', 'start thinking', 'Start Thinking'); ?></a></li>
        <li><a href="/album/map"><?php lang::str('global', 'map', 'Map'); ?></a></li>
        <li><a href="/album/think"><?php lang::str('global', 'thinkspot', 'ThinkSpot'); ?></a></li>
        <li><a href="/album/earth"><?php lang::str('global', 'earthspot', 'EarthSpot'); ?></a></li>
        <li><a href="/page/view?p=3"><?php lang::str('global', 'i am new', 'I am new'); ?></a></li>
        <?php if (!$data['user']->logged) { ?>
            <li><a href="/user/login"><?php lang::str('global', 'login', 'Login'); ?></a></li>
        <?php } else { ?>
            <li id="double">
                <a href="/user/account/panel"><?php lang::str('global', 'panel', 'Panel'); ?></a>
                <a href="/user/logout">Logout</a>
            </li>
            <?php
        }
        if (strlen($mes)) {
            ?>
            <li id="double"><a href="/contact"><?php lang::str('global', 'contact', 'Contact'); ?></a><?php echo $mes; ?></li>
            <?php
        } else {
            ?>
            <li><a href="/contact"><?php lang::str('global', 'contact', 'Contact'); ?></a></li>
            <?php
        }
        ?>
        <li class="download"><a href="/app/iphone"><?php lang::str('global', 'download_app', 'Download app'); ?></a></li>
    </ul>
    <form action="/action/user/lang" method="POST" id="lang">
        <label for="lang" class="totop">Language:</label>
        <select name='lang'>
            <option value='en_EN'>en-EN</option>
            <option value='nl_NL' <?php if (lang::getLang() == 'nl_NL') echo 'selected' ?>>nl-NL</option>
        </select>
    </form>
    <script>
        $('form#lang select').change(function() {
            $('form#lang').submit();
        });
    </script>
    <div id="logo">
        <a href="/"><img src="/style/images/logo.png"></a>
        <div class="slogan">Create value by sharing your reality</div>
    </div>
</div>
<?php
if (($data['arglist'] == 'album photo') OR
    ($data['arglist'] == 'album thinkspot')) {
    ?>
    <div id="userslist">
        <div id="naming">
            <?php lang::str('photo', 'latest members', 'Latest members'); ?>
        </div>
        <?php
        $re = $data['db']->query("SELECT * FROM users ORDER BY id DESC LIMIT 10");
        $res = mysql_fetch_array($re);
        do {

            if ($res['avatar'] != '') {
                $avatar = $res['avatar'];
            } else {
                $avatar = 'style/images/avatar.jpg';
            }
            echo "
<a id='user' href='/user/account?id={$res['id']}'>
<div id='avatar'><img src='/{$avatar}'></div>
<div id='name'>{$res['name']} {$res['surname']}</div>
</a>";
        } while ($res = mysql_fetch_array($re));
        ?>
    </div>
<?php } ?>