<div class="forgot">
    <h2 id='head'><?php lang::str('user', 'Forgot password', 'Forgot password'); ?></h2>
    <form action="/action/uforgot" method="POST">
        <div class="data line">
            <label for="password"><?php lang::str('user', 'password', 'Password'); ?></label>
            <input name="password" type="password" id="pass" class="pass" onkeyup="runPassword(this.value, 'pass');">
            <div id="pass_text"></div>
            <div class="res err password"></div>

        </div>
        <div class="data line">
            <label for="conf_password"><?php lang::str('user', 'password_repeat', 'Repeat password'); ?></label>
            <input name="conf_password" type="password" class="pass">

            <div class="res err conf_password"></div>

        </div>
        <div id='addit'><?php lang::str('user', 'robots protection', 'Robots protection: type the characters you see in the picture below'); ?></div>
        <div class="data line">
            <label></label>
            <img src="/kaptcha.php" id="captcha">
        </div>

        <div class="data line">
            <label for="captcha"></label>
            <input name="captcha" type="text" class="captcha">
            <div class="res err captcha"></div>
        </div>
        <div class="data line">
            <input type="submit" name="Send" value="<?php lang::str('global', 'send', 'Send'); ?>">
        </div>
        <div class="res success"></div>
        <input type="hidden" name="code" value="<?php echo $_GET['code']; ?>">
    </form>
</div>