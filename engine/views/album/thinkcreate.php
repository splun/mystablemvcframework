<form action="/action/thinkcreate" method="post" name="create" class="thcreate">
     <?php if(ISSET($data['controller']->info)) echo "<input name='id' type='hidden' value='{$data['controller']->info['id']}'>"; ?>
    <div class="data line">
        <label></label>
        <h2 id="head"><?php lang::str('photo', 'photo', 'Photo'); ?></h2>
    </div>
    <div class="data line">
        <label for="name"><?php lang::str('photo', 'photo name', 'Photo name'); ?></label>
        <input name="name" type="text" <?php if(ISSET($data['controller']->info)) echo ' value="'.$data['controller']->info['name'].'" '; ?>>
        <div class="res err name"></div>
    </div>
    <div class="data line">
        <label for="shortdesc"><?php lang::str('photo', 'short description', 'Description(short)'); ?></label>
        <textarea name="shortdesc"><?php if(ISSET($data['controller']->info)) echo $data['controller']->info['shortdesc']; ?></textarea>
        <div class="res err shortdesc"></div>
    </div>
    <div class="data line">
        <label for="description"><?php lang::str('photo', 'description', 'Description'); ?></label>
        <textarea name="description"><?php if(ISSET($data['controller']->info)) echo $data['controller']->info['description']; ?></textarea>
        <div class="res err description"></div>
    </div>

    <div class="data line">
        <label for="categories"><?php lang::str('photo', 'category', 'Category'); ?></label>
        <select name="categories">
            <?php
            $cat = new categories();
            $cats = $cat->getAll();
            foreach ($cats AS $key => $value) {
                $act = '';
                if(ISSET($data['controller']->info) AND ($data['controller']->info['category'] == $value['id'] )) $act = 'selected';;
                echo "
                        <option value='{$value['id']}' $act>{$value['name']}</option>";
            }
            ?>
        </select>
        <div class="res err categories"></div>
    </div>

    <div id='selected'>
        <?php
        if(ISSET($data['controller']->photos)){
            foreach($data['controller']->photos AS $photo){
                $photo['time'] = dateloc::dateTime($photo['time']);
                echo "
                    <div class='object {$photo['id']}'>
                    <input type='checkbox' name='photos[]' value='{$photo['id']}' checked>
                    <div class='obj'>
                        <div class = 'image'>
                            <a href='/album/photo?id={$photo['id']}' class='{$photo['id']}'><img src='{$photo['serv']}{$photo['thumb']}'></a>
                        </div>
                        <div = 'name'>{$photo['name']}</div>
                        <div id='time'>{$photo['time']}</div>
                    </div>
                    <a class='delete' id='{$photo['id']}'>delete</a>
                    </div>";
            }
        }
        ?>
    </div>
    <script>
        $("#selected").sortable();
    </script>
    <div class="data line">
        <label></label>
        <input type="submit" name="Send" value="<?php lang::str('global', 'send', 'Send'); ?>">
    </div>
    <div class="res success"></div>
</form>

<div class="map">
    <div id="map"></div>
    <form action ="/album/list" method="GET" class="search">
        <input type="hidden" name="x_top" value="">
        <input type="hidden" name="y_top" value="">
        <input type="hidden" name="x_bottom" value="">
        <input type="hidden" name="y_bottom" value="">
        <div class="selects">
            <div id="categories">
                <select name="categories">
                    <?php
                    $cat = new categories();
                    $cats = $cat->getAll();
                    foreach ($cats AS $key => $value) {
                        $act = '';
                        if (isset($_GET['cat']) AND ($_GET['cat'] == $value['id'])) {
                            $act = 'selected';
                        }
                        echo "
                        <option value='{$value['id']}' $act>{$value['name']}</option>";
                    }
                    ?>
                </select>
            </div>
        </div>
    </form>
    <div class="list"></div>
</div>
<div class="results">

</div>
<script>
    var map;
    var iw_opened = new google.maps.InfoWindow();
    var markers = [];
    var timer;
    var markerCluster;
    function add_marker(lat, lng, title, box_html, i) {
        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(lat, lng),
            map: map,
            title: title
        });
        google.maps.event.addListener(marker, 'click', function() {
            iw_opened.close();
            iw_opened = new google.maps.InfoWindow({content: box_html});
            iw_opened.open(map, marker);
        });
        return marker;
    }
    google.maps.Map.prototype.clearOverlays = function() {
        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(null);
        }
        if (markerCluster) {
            markerCluster.clearMarkers();
        }
        markers = new Array();
    }
    function imageShow(id, serv, thumb, name, x, y, time) {
        $('.map .list').append("<div class='obj'><div class= 'image'><a href='/album/photo?id=" + id + "' class='" + id + "'><img src='" + serv + thumb + "'></a></div><div = 'name'>" + name + "<\/div><div id='time'>" + time + "<\/div><\/div>");
    }
    $('form.search').submit(function() {
        $.get('/album/list', $('form.search').serializeArray(), function(json) {
            map.clearOverlays();
            $('.map .list').html('');
            for (var i = 0; i < json.markers.length; i++) {
                var baloon = "<div><a href=\'\/album\/photo?id=" + json.markers[i].id + "\'><img src='" + json.markers[i].serv + json.markers[i].thumb + "' height=\'100px\'><br>" + json.markers[i].name + "<\/a><\/div>";
                var marker = add_marker(json.markers[i].x, json.markers[i].y, json.markers[i].name, baloon, i);
                markers.push(marker);
                imageShow(json.markers[i].id, json.markers[i].serv, json.markers[i].thumb, json.markers[i].name, json.markers[i].x, json.markers[i].y, json.markers[i].time);
            }
            var mcOptions = {maxZoom: 12};
            markerCluster = new MarkerClusterer(map, markers, mcOptions);
        }, 'json');
    });
    function updatemap() {
        $('form.search').submit();
    }
    function initialize() {
        var mapOptions = {
            zoom: 8,
            center: new google.maps.LatLng(52.258698, 365.646441),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        map = new google.maps.Map(document.getElementById('map'), mapOptions);
        google.maps.event.addListener(map, 'bounds_changed', function() {
            clearTimeout(timer);
            var i = map.getBounds();
            timer = setTimeout(
                    function() {
                        $("form.search input[name=x_top]").val(i.getNorthEast().lat());
                        $("form.search input[name=y_top]").val(i.getNorthEast().lng());
                        $("form.search input[name=x_bottom]").val(i.getSouthWest().lat());
                        $("form.search input[name=y_bottom]").val(i.getSouthWest().lng());
                        updatemap();
                    }, 250);
        });
    }
    google.maps.event.addDomListener(window, 'load', initialize);
    $(document).ready(function() {
        $('form.search #categories select').change(function() {
            updatemap();
        });
        $('.map').on('click', 'a', function(e) {
            id = $(this).attr('class');
            if (!$('div').is("#selected .object." + id)) {
                text = $(this).parent().parent().html();
                $('#selected').prepend("<div class='object " + id + "'><input type='checkbox' name='photos[]' value='" + id + "' checked><div class='obj'>" + text + "</div><a class='delete' id='" + id + "'>delete</a></div>");

            }
            e.preventDefault();
            return false;
        });
        $("#selected").on('click', 'a.delete', function() {
            id = $(this).attr('id');
            $("#selected .object." + id).remove();
        });
    });
</script>