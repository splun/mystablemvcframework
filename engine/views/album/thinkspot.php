<div id='title'>
    <div id='name'>Think Spot: </div>
    <div id='data'><?php echo $data['controller']->thinkspot['name']; ?></div>
</div>
<div>
    <?php
    $data = data::init();
    if ($data['controller']->is) {
        ?>
        <div class="info">
            <div id='shortdesc'><?php echo $data['controller']->thinkspot['shortdesc']; ?></div>
            <div class="personal_page"><a href="/user/account?id=<?php echo $data['controller']->uid; ?>"><?php lang::str('photo', 'personal page', 'Personal<br>page'); ?></a></div>
            <div class="personal_page think"><a href="/album/thinkcreate"><?php lang::str('photo', 'create thinkspot', 'Create<br>thinkspot'); ?></a></div>
            <h2 id="title"><?php lang::str('photo', 'location', 'Location'); ?></h2>
            <div class="bigimage_map" id="map_<?php echo $data['controller']->id; ?>"></div>
            <div id='shortdesc'><?php echo $data['controller']->thinkspot['description']; ?></div>
        </div>
        <div class="image">
            <?php
            if($data['controller']->thinkspot['author'] == $data['user']->id){
                echo "<a href='/album/thinkcreate?id={$data['controller']->thinkspot['id']}' class='edit'>Edit Thinkspot</a>";
            }
            ?>
            <img src="<?php echo $data['controller']->image; ?>" class="bigimage">
            <h2 id="title"><?php echo $data['controller']->name; ?></h2>
            <div id="author"><?php lang::str('photo', 'author', 'Author'); ?> <a href='/user/account?id=<?php echo $data['controller']->owner; ?>' class='link'><?php echo $data['controller']->usurname; ?> <?php echo $data['controller']->uname; ?></a></div>
            <div id="date"><?php echo $data['controller']->date; ?></div>
            <div id="category">
                Themes:
                <a href='/album/think?cat=<?php echo $data['controller']->category; ?>' class='link'><?php $cat = new categories(); echo $cat->getName($data['controller']->category); ?></a><?php
                if(count($data['controller']->subcats)){
                    foreach($data['controller']->subcats AS $subcat){
                        $subcat['name'] = $cat->getName($subcat['id']);
                        echo ", <a href='/album/think?cat={$subcat['id']}' class='link'>{$subcat['name']}</a>";
                    }
                }
                ?>
            </div>
            <div id="description"><?php echo $data['controller']->description; ?></div>
            <?php if($data['controller']->photos){ ?>
            <div id="photosline" >
                <div class="container" style='width:<?php echo $r; ?>px;' >
                <?php foreach($data['controller'] -> photos AS $adphoto){ ?>
                <a href='/album/thinkspot?id=<?php echo "{$adphoto['id']}&group={$data['controller']->thinkspot['id']}"; ?>' width='75px'>
                    <img src='<?php echo "{$adphoto['serv']}{$adphoto['thumb']}"; ?>'>
                </a>
            <?php } echo '</div></div>';
            }
            if ($data['controller']->comallow) {
                ?>
                <div class="comments">
                    <div class="comment">
                        <a id="reply" class="0"><?php lang::str('photo', 'comment', 'Comment'); ?></a>
                        <div id="username" style="display:none;">Photo</div>
                        <div id="text"></div>
                    </div>
                    <?php
                    if (count($data['controller']->comments)) {
                        foreach ($data['controller']->comments as $key => $value) {
                            ?>
                            <div class="comment" id="<?php echo $value['id']; ?>" style="margin-left:<?php echo $value['level'] * 10; ?>px;">
                                <div id="avatar"><img src="/<?php echo $value['avatar']; ?>"></div>
                                <div id="inf">
                                    <a href="/user/account?id=<?php echo $value['unum']; ?>" id="username"><?php echo $value['name']; ?> <?php echo $value['surname']; ?></a>
                                    <div id="date"><?php echo $value['date']; ?></div>
                                    <div id="text"><?php echo $value['text']; ?></div>
                                </div>
                                <a id="reply" class='<?php echo $value['id']; ?>'><?php lang::str('photo', 'reply', 'Reply'); ?></a>
                            </div>
                            <?php
                        }
                        ?>
                        <div class="comment">
                            <a id="reply" class="0"><?php lang::str('photo', 'comment', 'Comment'); ?></a>
                            <div id="username" style="display:none;">Photo</div>
                            <div id="text"></div>
                        </div>
                        <?php
                    }
                    ?>


                    <form method="POST" action="/action/photo/comment" class="commentfrm">
                        <div id="replyto"></div>
                        <input type="hidden" name="photo" value="<?php echo $data['controller']->id; ?>">
                        <input type="hidden" name="parent" value="0">
                        <div class="data line">
                            <div class="res err logged"></div>
                        </div>
                        <div class="data line">
                            <textarea name="message" style="width:100%;"></textarea>
                            <div class="res err message"></div>
                        </div>
                        <div class="data line">
                            <input type="submit" name="Send" value="<?php lang::str('global', 'send', 'Send'); ?>">
                        </div>
                        <div class="res success"></div>
                    </form>
                </div>
        <?php } ?>
        </div>
        <?php
    } else {
        echo 'Photo not exists';
    }
    ?>


    <script type="text/javascript">
        function initialise() {
            var myLatlng = new google.maps.LatLng(
<?php echo $data['controller']->x_dest; ?>,
<?php echo $data['controller']->y_dest; ?>
    );
        var myOptions = {
            zoom: 13,
            center: myLatlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            panControl: false,
            zoomControl: false,
            scaleControl: true,
            mapTypeControl: false,
            scaleControl: false
        }
        var map_im = new google.maps.Map(document.getElementById("map_<?php echo $data['controller']->id; ?>"), myOptions);
                
        var marker = new google.maps.Marker({
            position: new google.maps.LatLng
            (<?php echo $data['controller']->x_dest; ?>,<?php echo $data['controller']->y_dest; ?>),
            map: map_im
        });
    }
    initialise();
        
    $('.comments a#reply').click(function(){
        var parent = $(this).attr("class");
        var form = $(this).parent();
        $('form.commentfrm').show(300);
        $('form.commentfrm input[name=parent]').attr('value',parent);
        $('form.commentfrm #replyto').html("<?php lang::str('photo', 'reply', 'Reply'); ?>: " + $("#username",form).html() + "<br>" + $("#text",form).html());
        $.scrollTo('form.commentfrm',500);
                    
    })
    
    $(document).ready(function(){
        $('form.commentfrm').ajaxForm({
            dataType:  'json',
            target: $(this).find('form'),
            beforeSubmit: function(data,$form){
                $('.res',$form).text('');
            },
            success: function(data,status,xhr,form){
                var parent = 0;
                $.each(data,function(key, val){
                    $('.res.'+key, form).text(val).show();
                    if(key == 'parent'){
                        parent = val;
                    }
                    if(key == 'comment'){
                        if(parent > 0){
                            var id = '#'+parent;
                            console.log(id);
                            $(val).insertAfter(id);
                        } else {
                            $(val).insertBefore('form.commentfrm');
                        }
                    }
                });
            }
        });
    })
    </script>
</div>