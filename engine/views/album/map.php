<div class="map">
    <div id="map"></div>
    <form action ="/album/list" method="GET" class="search">
        <input type="hidden" name="x_top" value="">
        <input type="hidden" name="y_top" value="">
        <input type="hidden" name="x_bottom" value="">
        <input type="hidden" name="y_bottom" value="">
    </form>
</div>
<div class="results">

</div>
<script>
    var map;
    var iw_opened = null;
    var markers = [];
    var timer;
    var markerCluster;
    
    function add_marker(lat, lng, title, box_html, i) {
        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(lat, lng),
            map: map,
            title: title
        });
        google.maps.event.addListener(marker, 'click', function () {
            $('.results').html(box_html);
            $('.results').show();
        });
        return marker;
    }
    
    google.maps.Map.prototype.clearOverlays = function() {
        for (var i = 0; i < markers.length; i++ ) {
            markers[i].setMap(null);
        }
        if(markerCluster){
            markerCluster.clearMarkers();
        }
        markers = new Array();
    };
    $('form.search').submit(function(){
        $.get('/album/list',$('form.search').serializeArray(),function(json){
            map.clearOverlays();
            for (var i = 0; i < json.markers.length; i++) {
             
                subcats = 'Categories:<br><a href=\'\/album\/think?cat='+json.markers[i].subcat[0].id+
                            '\' class=\'link\'>'+json.markers[i].subcat[0].name+'<\/a>';
                console.log(json.markers[i].subcat.length);
                for(var j = 1; j < json.markers[i].subcat.length; j++){
                    subcats = subcats+', <a href=\'\/album\/think?cat='+json.markers[i].subcat[j].id+
                            '\' class=\'link\'>'+json.markers[i].subcat[j].name+'<\/a>';
                }
                var baloon = "<a href=\'\/album\/photo?id=" + json.markers[i].id + 
                        "\'><h2 id=\'head\'>" + json.markers[i].name + 
                        "</h2><img src='" + json.markers[i].serv + 
                        json.markers[i].file + "' ><\/a><div id=\'info\'>" + json.markers[i].added + 
                        "<br>Author: <a href=\'\/user\/account?id=" + json.markers[i].owner + "\' class=\'link\'>" + 
                        json.markers[i].uname + " " + json.markers[i].usurname + "<\/a><div id=\'categories\'>" +
                        subcats + "<\/div><div id=\'desc\'>" + json.markers[i].description + 
                        "<\/div><\/div>";
                var marker = add_marker(json.markers[i].x, json.markers[i].y, json.markers[i].name, baloon,i);
                markers.push(marker);
            }
            var mcOptions = {maxZoom: 12};
            markerCluster = new MarkerClusterer(map, markers, mcOptions);            
        }, 'json');
    });
    function updatemap(){
        $('form.search').submit();
    }
    function initialize() {
        var mapOptions = {
            zoom: 8,
            center: new google.maps.LatLng(52.258698,365.646441),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        map = new google.maps.Map(document.getElementById('map'), mapOptions);
        google.maps.event.addListener(map, 'bounds_changed', function() {
            clearTimeout(timer);
            var i = map.getBounds();
            timer = setTimeout(
            function() {
                $("form.search input[name=x_top]").val(i.getNorthEast().lat());
                $("form.search input[name=y_top]").val(i.getNorthEast().lng());
                $("form.search input[name=x_bottom]").val(i.getSouthWest().lat());
                $("form.search input[name=y_bottom]").val(i.getSouthWest().lng());
                updatemap();
            },250);
        });
    }
    google.maps.event.addDomListener(window, 'load', initialize);
</script>