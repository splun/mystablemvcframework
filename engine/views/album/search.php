<div class="map">
    <div id="map"></div>
    <form action ="/album/list" method="GET" class="search">
        <input type="hidden" name="x_top" value="">
        <input type="hidden" name="y_top" value="">
        <input type="hidden" name="x_bottom" value="">
        <input type="hidden" name="y_bottom" value="">
        <div class="selects">
            <div id="categories">
                <select name="categories">
                    <option value='0'>Общая категория 1</option>
                    <option value='1'>Общая категория 2</option>
                    <option value='2'>Общая категория 3</option>
                    <option value='3'>Общая категория 4</option>
                </select>
            </div>
        </div>
    </form>
</div>
<div class="results">

</div>
<script>
    var map;
    var iw_opened = null;
    var markers = [];
    var timer;
    var markerCluster;
    
    function add_marker(lat, lng, title, box_html, i) {
        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(lat, lng),
            map: map,
            title: title
        });
        google.maps.event.addListener(marker, 'click', function () {
            iw_opened = new google.maps.InfoWindow({content:box_html});
        });
        return marker;
    }
    
    google.maps.Map.prototype.clearOverlays = function() {
        for (var i = 0; i < markers.length; i++ ) {
            markers[i].setMap(null);
        }
        if(markerCluster){
            markerCluster.clearMarkers();
        }
        markers = new Array();
    }
    
    function imageShow(id, serv, thumb, name, x, y){
        $('.content .results').append("<div class='obj'><div class= 'image'><a href='/album/photo?id=" + id + "'><img src='" + serv + thumb + "'></a></div><div = 'name'>" + name + "<\/div><\/div>");
    }
    $('form.search').submit(function(){
        $.get('/album/list',$('form.search').serializeArray(),function(json){
            map.clearOverlays();
            for (var i = 0; i < json.markers.length; i++) {
                var baloon = "<div><a href=\'\/album\/photo?id="+ json.markers[i].id +"\'><img src='" + json.markers[i].serv + json.markers[i].file + "' height=\'300px\'><br>" + json.markers[i].name + "<\/a><\/div>";
                var marker = add_marker(json.markers[i].x, json.markers[i].y, json.markers[i].name, baloon,i);
                markers.push(marker);
                <?php //imageShow(json.markers[i].id, json.markers[i].serv, json.markers[i].thumb, json.markers[i].name, json.markers[i].x, json.markers[i].y); ?>
            }
            var mcOptions = {maxZoom: 12};
            markerCluster = new MarkerClusterer(map, markers, mcOptions);            
        }, 'json');
    });
    function updatemap(){
        $('form.search').submit();
    }
    function initialize() {
        var mapOptions = {
            zoom: 8,
            center: new google.maps.LatLng(52.258698,365.646441),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        map = new google.maps.Map(document.getElementById('map'), mapOptions);
        google.maps.event.addListener(map, 'bounds_changed', function() {
            clearTimeout(timer);
            var i = map.getBounds();
            timer = setTimeout(
            function() {
                $("form.search input[name=x_top]").val(i.getNorthEast().lat());
                $("form.search input[name=y_top]").val(i.getNorthEast().lng());
                $("form.search input[name=x_bottom]").val(i.getSouthWest().lat());
                $("form.search input[name=y_bottom]").val(i.getSouthWest().lng());
                updatemap();
            },250);
        });
    }
    google.maps.event.addDomListener(window, 'load', initialize);
    $(document).ready(function(){
        $('#categories select').change(function() {
            updatemap();
        });
    });
</script>