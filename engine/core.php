<?php
require_once 'autoload.php';
require_once 'functions.php';
$data = data::init();
$route = new router();
$db = new Db_Mysql();
$data->set('router',$route);
$data->set('db',$db);
$data->set('user',new user());
$data['user']->setActive();
$route->setPath(PATH_CORE);
$route->delegate();
$user = new user();
loadController("page");
loadView("html_page");
?>