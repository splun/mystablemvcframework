<?php

class Controllers_action_message Extends Controllers_Base {

    var $reqlogged = true;

    public function index() {
        
    }

    function __construct() {
        $data = data::init();
        $errors = null;
        if (!ISSET($_POST['text']) OR (strlen($_POST['text']) < 1)) {
            $errors['message'] = lang::getStr('error', 'no_message', 'Please input message');
        }
        if (!ISSET($_POST['to'])) {
            $errors['to'] = lang::getStr('error', 'no_login', 'Please select user to');
        }
        if (count($errors)) {
            die(json_encode($errors));
        } else {
            $from = $data['user']->id;
            $to = protection::number($_POST['to']);
            $message = $_POST['text'];
            $time = time();
            $data['db']->query("INSERT INTO messages (`from`, `to`, `message`, `time`) VALUES ('$from','$to','$message','$time')");

            $resd = $data['db']->query("
SELECT
	messages.`from` AS `from`,
	messages.`time` AS `time`,
	messages.`message` AS `message`,
	users.`avatar` AS `avatar`,
	users.`name` AS `name`,
	users.`surname` AS `surname`
FROM messages INNER JOIN users ON users.`id` = messages.`from`
WHERE messages.`to` = $to AND messages.`from` = $from AND messages.`time` = $time
LIMIT 1");

            while ($value = mysql_fetch_array($resd)) {
                if ($value['avatar'] == '') {
                    $value['avatar'] = 'style/images/avatar.jpg';
                }
                $res = "
            <div id='message' class='notread'>
                <div id='avatar'><img src='/" . "{$value['avatar']}'></div>
                <div id='name'><a href='/user/account?id={$value['from']}'>from: {$value['name']}{$value['surname']}</a></div>
                <div id='time'>" . dateloc::datetime($value['time']) . "</div>
                <div id='text'>{$value['message']}</div>
            </div>";
            }
            die(json_encode(array('success' => "Successfully send", 'text' => $res)));
        }
    }

}
?>