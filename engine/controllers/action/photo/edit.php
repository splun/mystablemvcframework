<?php

Class Controllers_action_photo_edit Extends Controllers_Base {

    function __construct() {
        $data = data::init();
        $errors = null;
        if (!$data['user']->logged) {
            $errors['logged'] = lang::getStr('error', 'not_logged', 'Not logged in');
        }
        if (count($errors)) {
            die(json_encode($errors));
        } else {
            if($_POST['acctype']>0){
                $data['db']->query("DELETE FROM `allowed` WHERE `group`='{$_POST['acctype']}'");
                $list = array();
                foreach ($_POST['usr'] AS $key => $value){
                    $list[] = "('{$_POST['acctype']}','$value')";
                }
                $query = "INSERT INTO `allowed`(`group`,`user`) VALUES".implode(',',$list);
                $data['db']->query($query);
            }
            $user = $data['user']->id;
            $r = $data['db']->query("SELECT * FROM photos WHERE id='{$_POST['pid']}' AND owner={$data['user']->id} LIMIT 1");
            if(mysql_num_rows($r)){
                $r = mysql_fetch_array($r);
                if(!$r['status']){
                    $q = "UPDATE photos SET name = '{$_POST['name']}', description = '{$_POST['description']}', allowed='{$_POST['acctype']}', category='{$_POST['categories']}' WHERE id='{$_POST['pid']}' LIMIT 1";
                } else {
                    $q = "UPDATE photos SET allowed='{$_POST['acctype']}' WHERE id='{$_POST['pid']}' LIMIT 1";
                }
            }
            $data['db']->query($q);
            die(json_encode(array('success' => lang::getStr('success', 'photo_saved', 'Photo saved'))));
        }
    }

    public function index() {
        
    }

}

?>