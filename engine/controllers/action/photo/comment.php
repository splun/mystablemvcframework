<?php

Class Controllers_action_photo_comment Extends Controllers_Base {

    function __construct() {
        $data = data::init();
        $photo = protection::number($_POST['photo']);
        $errors = null;
        if (!$data['user']->logged) {
            $errors['logged'] = lang::getStr('error', 'not_logged', 'Not logged in');
        }
        if (strlen($_POST['message']) < 1) {
            $errors['message'] = lang::getStr('error', 'message_empty', 'Message empty');
        }
        $allow = $data['db']->query('SELECT allowed FROM photos WHERE id=' . $photo . ' LIMIT 1');
        $allow = mysql_fetch_array($allow);
        if ($allow['allowed']) {
            $allow = $data['db']->query("SELECT * FROM `allowed` WHERE `group`={$allow['allowed']} AND `user`={$data['user']->id} LIMIT 1");
            if (!mysql_num_rows($allow)) {
                $errors['message'] = lang::getStr('error', 'message_not_allowed', 'You are not allowed');
            }
        }
        if (count($errors)) {
            die(json_encode($errors));
        } else {
            $message = protection::text($_POST['message']);

            $user = $data['user']->id;
            $time = time();
            $parent = protection::number($_POST['parent']);
            $res = $data['db']->query("INSERT INTO comments(user,photo,text,date,com_parent) VALUES ({$user},{$photo},'{$message}',$time,$parent)");
            $id = mysql_insert_id();
            $time = dateloc::dateTime($time);
            $com = "<div class='comment' id='$id'>
                                <div id='avatar'><img src='/{$data['user']->data['avatar']}'></div>
                                <div id='inf'>
                                    <a href='/user/account?id={$data['user']->id}' id='username'>{$data['user']->data['name']} {$data['user']->data['surname']}</a>
                                    <div id='date'>$time</div>
                                    <div id='text'>$message</div>
                                </div>
                                <a id='reply' class='$id'>Reply</a>
                            </div>";
            die(json_encode(array('success' => lang::getStr('success', 'comment_added', 'Comment added'), 'parent' => $parent, 'comment' => $com)));
        }
    }

    public function index() {
        
    }

}

?>