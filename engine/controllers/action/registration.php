<?php
Class Controllers_action_registration Extends Controllers_Base {
    function __construct() {
        $data = data::init();
        $errors = null;
        if (!ISSET($_POST['login']) OR (strlen($_POST['login']) < 1)) {
            $errors['login'] = lang::getStr('error', 'no_login', 'Please input login');
        }
        if (!ISSET($_POST['password']) OR (strlen($_POST['password']) < 1)) {
            $errors['password'] = lang::getStr('error', 'no_password', 'Please input password');
        }
        if ($_POST['conf_password'] != $_POST['password']) {
            $errors['conf_password'] = lang::getStr('error', 'pass_no_match', 'Passwords don\'t match');
        }
        if (!ISSET($_POST['mail']) OR (strlen($_POST['mail']) < 1)) {
            $errors['mail'] = lang::getStr('error', 'no_mail', 'Please input mail');
        }
        if (($timestamp = strtotime($_POST['birthday'])) === false) {
            $errors['birthday'] = lang::getStr('error', 'birthday_format_error', 'Birthday format error. Required like 30/10/2000 or 06-10-2011');
        }
        if (!protection::mail($_POST['mail'])) {
            $errors['mail'] = lang::getStr('error', 'mail_format_error', 'Mail format error');
        }
        if (!count($errors)) {
            $login = protection::username($_POST['login']);
            if (mysql_num_rows($data['db']->query("SELECT id FROM users WHERE login = '{$login}' LIMIT 1"))) {
                $errors['login'] = lang::getStr('error', 'login_isset', 'Login already registered');
            }
            if (mysql_num_rows($data['db']->query("SELECT id FROM users WHERE mail = '{$_POST['mail']}' LIMIT 1"))) {
                $errors['mail'] = lang::getStr('error', 'mail_isset', 'Mail already registered');
            }
        }
        if (count($errors)) {
            die(json_encode($errors));
        } else {
            $login = protection::username($_POST['login']);
            $pass = md5($_POST['password']);
            $mail = $_POST['mail'];
            $country = protection::number($_POST['country']);
            $name = protection::string($_POST['name']);
            $surname = protection::string($_POST['surname']);
            $key = new key();
            $key = $key->authkey($login,$pass,$mail);
            $data['db']->query("INSERT INTO users(login,password,mail,code,name,surname,birth,country) VALUES ('{$login}','{$pass}','{$mail}','{$key}','{$name}','{$surname}','{$timestamp}','{$country}')");
            $rmail = new smail();
            $rmail->mail_utf8($mail, '', 'admin@earthinks.com', 'Регистрация', 'Успешно. Подтвердите вашу почту по ссылке <a href="http://earthinks.ctydio21test.com.ua/action/uapp?code=' . $key . '">http://earthinks.ctydio21test.com.ua/action/uapp?code=' . $key . '</a>');
            die(json_encode(array('success' => 'Successfully registered. Approvement code sent to your email')));
        }
    }
    public function index() {
        
    }
}
?>