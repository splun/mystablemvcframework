<?php
Class Controllers_action_thinkcreate Extends Controllers_Base {
    var $reqlogged = true;
    function __construct(){
        $data = data::init();
        $errors = null;
        if(strlen($_POST['name']) < 1){
            $errors['name'] = 'No name';
        }
        if(strlen($_POST['description']) < 1){
            $errors['description'] = 'No description';
        }
        if(strlen($_POST['shortdesc']) < 1){
            $errors['shortdesc'] = 'No short description';
        }
        if (count($errors)) {
            die(json_encode($errors));
        } else {
            $time = time();
            $desc = htmlspecialchars($_POST['description']);
            $name = htmlspecialchars($_POST['name']);
            $shortdesc = htmlspecialchars($_POST['shortdesc']);
            if(ISSET($_POST['id'])){
                $thinkspot = $_POST['id'];
                $q1 = "UPDATE thinkspot SET `name` = '{$name}', `description` = '{$desc}', `shortdesc` = '{$shortdesc}', `avatar` = '{$_POST['photos'][0]}', `category` = '{$_POST['categories']}' WHERE `id` = '{$thinkspot}'";
                $data['db']->query($q1);
            } else {
                $q1 = "INSERT INTO thinkspot(`name`,`description`,`created`,`author`,`shortdesc`,`avatar`,`category`) VALUES ('{$name}','{$desc}','{$time}','{$data['user']->id}','{$shortdesc}','{$_POST['photos'][0]}','{$_POST['categories']}')";
                $data['db']->query($q1);
                $thinkspot = mysql_insert_id();
            }
            $data['db']->query("DELETE FROM think_photos WHERE `group` = '{$thinkspot}'");
            foreach($_POST['photos'] AS $photo){
                $q2 = "INSERT INTO think_photos(`photo`,`group`) VALUES('{$photo}','{$thinkspot}')";
                $data['db']->query($q2);
            }
            die(json_encode(array('success'=> "Successfully done. You can view thinkspot <a href='/album/thinkspot?id={$_POST['photos'][0]}&group={$thinkspot}'>here</a>")));
        }
    }
    public function index() {
        
    }    
}