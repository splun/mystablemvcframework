<?php

Class Controllers_action_load Extends Controllers_Base {

    function __construct() {
        $data = data::init();

        $errors = null;
        if (!isset($_POST) || empty($_POST)) {
            $errors['data'] = lang::getStr('error', 'no_parametrs', 'No parametrs');
            die(json_encode($errors));
        }
        $userName = protection::username($_POST['user']);
        $userPassword = protection::password($_POST['pass']);
        $userXcoord = protection::number($_POST['xcoord']);
        $userYcoord = protection::number($_POST['ycoord']);
        $userComment = protection::text($_POST['comment']);
        $userCategory = protection::number($_POST['category']);
        $uploadFile = basename($_FILES['userfile']['name']);
        $randomNumber = rand(0, 99999);
        $uploaddir = '/home/ctydio21/ctydio21test.com.ua/earthinks/';
        $fileinfo = pathinfo($_FILES['userfile']['name']);
        $fname = 'files/images/' . time() . $randomNumber . '.' . $fileinfo['extension'];
        $tname = 'files/thumbs/' . time() . $randomNumber . '.' . $fileinfo['extension'];
        $newName = $uploaddir . $fname;

        if (!is_uploaded_file($_FILES['userfile']['tmp_name'])) {
            $errors['userfile'] = lang::getStr('error', 'not_uploaded', 'Not uploaded');
        }
        if (!$userName || !$userPassword) {
            $errors['user'] = lang::getStr('error', 'not_logged', 'Not logged');
        } else {
            $userPassword = md5($userPassword);
            $res = $data['db']->query("SELECT id FROM users WHERE login='{$userName}' AND password='{$userPassword}' LIMIT 1");
            if (!mysql_num_rows($res)) {
                $errors['user'] = lang::getStr('error', 'no_user', 'User not exists');
            } else {
                $res = mysql_fetch_array($res);
                $id = $res['id'];
            }
        }
        if (!$userXcoord) {
            $errors['xcoord'] = lang::getStr('error', 'no_xcoord', 'Not correct xcoord');
        }
        if (!$userYcoord) {
            $errors['ycoord'] = lang::getStr('error', 'no_ycoord', 'Not correct ycoord');
        }
        if (count($errors)) {
            die(json_encode($errors));
        } else {
            if (move_uploaded_file($_FILES['userfile']['tmp_name'], $newName)) {
                
                $image = imagecreatefromstring(file_get_contents($newName));
                $exif = exif_read_data($newName);
                if (!empty($exif['Orientation'])) {
                    switch ($exif['Orientation']) {
                        case 8:
                            $image = imagerotate($image, 90, 0);
                            imagejpeg($image,$newName);
                            break;
                        case 3:
                            $image = imagerotate($image, 180, 0);
                            imagejpeg($image,$newName);
                            break;
                        case 6:
                            $image = imagerotate($image, -90, 0);
                            imagejpeg($image,$newName);
                            break;
                    }
                }
                
                $postsize = ini_get('post_max_size');
                $canupload = ini_get('file_uploads');
                $tempdir = ini_get('upload_tmp_dir');
                $maxsize = ini_get('upload_max_filesize');
                loadClass('imageFile');
                $imPrev = new imageFile($fname);
                $imPrev->imageresize($uploaddir . $tname, 200, 200, 100);
                $added = time();
                $data['db']->query("INSERT INTO photos(owner,x_dest,y_dest,file,thumb,description,category,added,serv) VALUES ('$id','$userXcoord','$userYcoord','$fname','$tname','$userComment','$userCategory','$added','http://earthinks.ctydio21test.com.ua/')");
                die(json_encode(array('success' => 'Successfully uploaded')));
            }
            die(json_encode(array('userfile' => 'Unknown error')));
        }
    }

    public function index() {
        
    }

}

?>