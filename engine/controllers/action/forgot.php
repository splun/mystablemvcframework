<?php

Class Controllers_action_forgot Extends Controllers_Base {

    var $data;

    function __construct() {
        $data = data::init();
        $errors = array();
        if (!ISSET($_POST['mail']) OR (strlen($_POST['mail']) < 1)) {
            $errors['mail'] = lang::getStr('error', 'no_mail', 'Please input mail');
        }
        if (!protection::mail($_POST['mail'])) {
            $errors['mail'] = lang::getStr('error', 'mail_format_error', 'Mail format error');
        }
        
        if (isset($_SESSION['captcha_keystring']) && $_SESSION['captcha_keystring'] === $_POST['captcha']) {} else {
            $errors['captcha'] = lang::getStr('error', 'captcha_format_error', 'Captcha error');
        }
        unset($_SESSION['captcha_keystring']);
        if (count($errors)) {
            die(json_encode($errors));
        } else {
            $mail = $_POST['mail'];
            
            $re = $data['db']->query("SELECT code FROM users WHERE mail = '$mail' LIMIT 1");
            if(mysql_num_rows($re)){
                $re = mysql_fetch_array($re);
                $key = $re['code'];
                $key = substr($key, rand(1,10), rand(30,40));
                $re = $data['db']->query("INSERT INTO forgot_queries(code,user,mail) VALUES ('$key','{$data['user']->id}','$mail')");
                $rmail = new smail();
                $rmail->mail_utf8(
                        $mail,
                        '',
                        'admin@earthinks.com',
                        'Forgot password',
                        "Follow the link to procceed <a href='http://earthinks.ctydio21test.com.ua/uforgot?code=$key'>http://earthinks.ctydio21test.com.ua/uforgot?code=$key</a>"
                        );
            }
            //$data['db']->query('INSERT INTO contact_mess(mail,text,time,user) VALUES ("'.$mail.'","'.$text.'","'.time().'","'.$data['user']->id.'")');
            die(json_encode(array('success' => 'Successfully sent')));
        }
    }

    public function index() {
        
    }

}

?>