<?php

Class Controllers_action_accsettings Extends Controllers_Base {

    var $reqlogged = true;

    function __construct() {
        $data = data::init();
        $errors = null;
        $avaname = '';
        if (($timestamp = strtotime($_POST['birth'])) === false) {
            $errors['birth'] = lang::getStr('error', 'birthday_format_error', 'Birthday format error. Required like 30/10/2000 or 06-10-2011');
        }
        if (!ISSET($_POST['curpassword']) OR (strlen($_POST['curpassword']) < 1)) {
            $errors['curpassword'] = lang::getStr('error', 'no_password', 'Please input password');
        }
        $p = md5($_POST['curpassword']);
        $re = $data['db']->query("SELECT * FROM users WHERE id = '{$data['user']->id}' AND password = '{$p}' LIMIT 1");
        if (!mysql_num_rows($re)) {
            $errors['curpassword'] = lang::getStr('error', 'invalid_password', 'Invalid password');
        }
        if (ISSET($_POST['pass_change']) AND $_POST['pass_change'] == 'on') {
            if ($_POST['conf_password'] != $_POST['password']) {
                $errors['conf_password'] = lang::getStr('error', 'pass_no_match', 'Passwords don\'t match');
            } else {
                if (mysql_num_rows($re)) {
                    $re = mysql_fetch_array($re);
                    $pass = $_POST['password'];
                    $key = new key();
                    $key = $key->authkey($re['login'], $pass, $re['mail']);
                    setcookie('auth_key', $key, time() + 60 * 60 * 24 * 365, '/');
                }
            }
        }
        if (isset($_SESSION['captcha_keystring']) && $_SESSION['captcha_keystring'] === $_POST['captcha']) {
            
        } else {
            $errors['captcha'] = lang::getStr('error', 'captcha_format_error', 'Captcha error');
        }
        //unset($_SESSION['captcha_keystring']);
        if (count($errors)) {
            die(json_encode($errors));
        } else {
            $name = protection::string($_POST['name']);
            $surname = protection::string($_POST['surname']);
            $addinfo = $_POST['addinfo'];
            $k = '';



            if (is_uploaded_file($_FILES['avatar']['tmp_name'])) {
                $res = mysql_fetch_array($re);
                $randomNumber = rand(0, 99999);
                $fileinfo = pathinfo($_FILES['avatar']['name']);
                $avaname = 'files/avatars/' . time() . $randomNumber . '.' . $fileinfo['extension'];
                loadClass('imageFile');
                $imPrev = new imageFile($_FILES['avatar']['tmp_name']);
                $imPrev->imageresize(PATH_ROOT . $avaname, 200, 200, 100);
                if ($res['avatar'] != '') {
                    unlink(PATH_ROOT . $res['avatar']);
                }
                $k = "$k, avatar = '$avaname'";
            }

            if (ISSET($key)) {
                $pas = md5($_POST['password']);
                $k = "$k, code = '$key', password='{$pas}'";
            }
            $query = "UPDATE users SET name = '$name', surname = '$surname', addinfo = '$addinfo', birth = '$timestamp'$k WHERE id = '{$data['user']->id}' LIMIT 1";
            $re = $data['db']->query($query);
            die(json_encode(array('success' => 'Successfully updated')));
        }
    }

    public function index() {
        
    }

}

?>