<?php

Class Controllers_action_approve Extends Controllers_Base {
    var $reqlogged = true;
    function __construct() {
        $data = data::init();

        $errors = null;
        if (!ISSET($_POST)) {
            $errors['fail'] = lang::getStr('error', 'no_logged', 'Please login');
        }
        if (count($errors)) {
            die(json_encode($errors));
        } else {
            $data = data::init();
            if ($_POST['act'] == 1) {
                $name = protection::string($_POST['name']);
                $x_dest = protection::number($_POST['x_dest']);
                $y_dest = protection::number($_POST['y_dest']);
                $description = protection::text($_POST['description']);
                $data['db']->query("UPDATE photos SET status = 1, name = '$name', description = '$description', x_dest = '$x_dest' , y_dest = '$y_dest' WHERE id={$_POST['photo']} LIMIT 1");
                die(json_encode(array('success' => 'Approved successfully')));
            } else if($_POST['act'] == 2) {
                $photo = protection::number($_POST['photo']);
                $res = $data['db']->query("SELECT * FROM photos WHERE id={$photo} LIMIT 1");
                $res = mysql_fetch_array($res);
                $uploaddir = '/home/ctydio21/ctydio21test.com.ua/earthinks/';
                $newName = $uploaddir.$res['file'];
                $fname = basename($newName);
                
                loadClass('imageFile');
                
                $im = new imageFile($res['file']);
                $im->save($newName, $uploaddir);
                
                die(json_encode(array('success' => 'Recreated successfully')));
            } else {
                $photo = protection::number($_POST['photo']);
                $res = $data['db']->query("SELECT * FROM photos WHERE id={$photo} LIMIT 1");
                $res = mysql_fetch_array($res);
                $uploaddir = '/home/ctydio21/ctydio21test.com.ua/earthinks/';
                $thumb = $uploaddir.$res['thumb'];
                $file = $uploaddir.$res['file'];
                if(file_exists($file)){
                    unlink($file);
                }
                if(file_exists($thumb)){
                    unlink($thumb);
                }
                $data['db']->query("DELETE FROM photos WHERE id={$photo} LIMIT 1");
                $data['db']->query("DELETE FROM comments WHERE photo={$photo}");
                die(json_encode(array('success' => 'Deleted successfully')));
            }

            
        }
    }
    public function index() {
        
    }
}
?>