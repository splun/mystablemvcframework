<?php
Class Controllers_action_contact Extends Controllers_Base {
    var $data;
    function __construct() {
        classOut(__CLASS__);
        $data = data::init();
        $errors = null;
        if (!ISSET($_POST['mail']) OR (strlen($_POST['mail']) < 1)) {
            $errors['mail'] = lang::getStr('error', 'no_mail', 'Please input mail');
        }
        if (!protection::mail($_POST['mail'])) {
            $errors['mail'] = lang::getStr('error', 'mail_format_error', 'Mail format error');
        }
        
        if (isset($_SESSION['captcha_keystring']) && $_SESSION['captcha_keystring'] === $_POST['captcha']) {} else {
            $errors['captcha'] = lang::getStr('error', 'captcha_format_error', 'Captcha error');
        }
        unset($_SESSION['captcha_keystring']);
        if (count($errors)) {
            die(json_encode($errors));
        } else {
            $mail = $_POST['mail'];
            $text = $_POST['question'];
            $data['db']->query('INSERT INTO contact_mess(mail,text,time,user) VALUES ("'.$mail.'","'.$text.'","'.time().'","'.$data['user']->id.'")');
            die(json_encode(array('success' => 'Successfully sent')));
        }
    }
    public function index() {
        
    }
}
?>