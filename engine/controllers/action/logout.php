<?php
Class Controllers_action_logout Extends Controllers_Base {

    function __construct() {
        setcookie('auth_key', '', 0, '/');
        die(json_encode(array('success' => 'Successfully logged out', 'reload' => 'true')));
    }

    public function index() {
        
    }

}
?>
