<?php
Class Controllers_action_login Extends Controllers_Base {
    var $data;
    function __construct() {
        $data = data::init();
        $errors = null;
        if (!ISSET($_POST['login']) OR (strlen($_POST['login']) < 1)) {
            $errors['login'] = lang::getStr('error', 'no_login', 'Please input login');
        }
        if (!ISSET($_POST['password']) OR (strlen($_POST['password']) < 1)) {
            $errors['password'] = lang::getStr('error', 'no_password', 'Please input password');
        }
        if (count($errors)) {
            die(json_encode($errors));
        } else {
            $login = protection::username($_POST['login']);
            $pass = md5(protection::password($_POST['password']));
            $res = $data['db']->query("SELECT id, code FROM users WHERE login = '{$login}' AND password = '$pass' LIMIT 1");
            if (mysql_num_rows($res)) {
                $res = mysql_fetch_array($res);
                $time = 0;
                if(ISSET($_POST['remember']) AND $_POST['remember'] == 'on'){
                    $time = time() + 60 * 60 * 24 * 365;
                } else {
                    $time = time() + 60 * 60 * 24;
                }
                setcookie('auth_key', $res['code'], $time, '/');
                die(json_encode(array('success'=> 'Successfully logged in', 'reload' => '/user/account/panel')));
            }
            else {
                die(json_encode(array('login'=> lang::getStr('error', 'no_user', 'User not exists'))));
            }
        }
    }
    public function index() {
        
    }
}
?>