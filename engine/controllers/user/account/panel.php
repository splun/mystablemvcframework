<?php

class Controllers_user_account_panel Extends Controllers_Base {

    var $info;
    var $notconf;
    var $conf;
    var $reqlogged = true;
    var $messages;
    var $mesnew;
    var $earthspots;

    function __construct() {
        $data = data::init();
        $re = $data['db']->query("SELECT * FROM users WHERE id = '{$data['user']->id}' LIMIT 1");
        if (mysql_num_rows($re)) {
            $re = mysql_fetch_array($re);
            $dat = array();
            $dat['name'] = $re['name'];
            $dat['surname'] = $re['surname'];
            $dat['avatar'] = $re['avatar'];
            $dat['addinfo'] = $re['addinfo'];
            if ($re['birth']) {
                $dat['birth'] = date('d-m-Y', $re['birth']);
            } else {
                $dat['birth'] = '';
            }
            $this->info = $dat;
        }


        $res = $data['db']->query("SELECT * FROM photos WHERE owner = '{$data['user']->id}'");
        $conf = array();
        $notconf = array();

        if (mysql_num_rows($res)) {
            $res1 = mysql_fetch_array($res);
            $i = 0;
            do {
                if ($res1['status'] == 0) {
                    $notconf[$i]['owner'] = $res1['owner'];
                    $notconf[$i]['id'] = $res1['id'];
                    $notconf[$i]['serv'] = $res1['serv'];
                    $notconf[$i]['file'] = $res1['file'];
                    $notconf[$i]['description'] = $res1['description'];
                    $notconf[$i]['x_dest'] = $res1['x_dest'];
                    $notconf[$i]['y_dest'] = $res1['y_dest'];
                    $notconf[$i]['name'] = $res1['name'];
                    $notconf[$i]['category'] = $res1['category'];
                    $notconf[$i]['thumb'] = $res1['thumb'];
                    $i++;
                } else {
                    $conf[$i]['owner'] = $res1['owner'];
                    $conf[$i]['id'] = $res1['id'];
                    $conf[$i]['serv'] = $res1['serv'];
                    $conf[$i]['file'] = $res1['file'];
                    $conf[$i]['description'] = $res1['description'];
                    $conf[$i]['x_dest'] = $res1['x_dest'];
                    $conf[$i]['y_dest'] = $res1['y_dest'];
                    $conf[$i]['name'] = $res1['name'];
                    $conf[$i]['category'] = $res1['category'];
                    $conf[$i]['thumb'] = $res1['thumb'];
                    $i++;
                }
            } while ($res1 = mysql_fetch_array($res));
            $this->conf = $conf;
            $this->notconf = $notconf;
            $mesnew = query_to_array($data['db']->query("SELECT COUNT(*) as cnt FROM messages WHERE `to` = '{$data['user']->id}' AND `read` = '0'"));
            $this->mesnew = $mesnew[0]['cnt'];
            $messagesto = query_to_array($data['db']->query("SELECT * FROM( SELECT messages.`from` AS `from`, messages.`read` AS `read`, messages.`time` AS `time`, messages.`message` AS `message`, users.`avatar` AS `avatar`, users.`name` AS `name`, users.`surname` AS `surname` FROM messages INNER JOIN users ON users.`id` = messages.`from` WHERE messages.`to` = {$data['user']->id} ORDER BY messages.`id` DESC) AS x GROUP BY `from`"));
            $messagesfrom = query_to_array($data['db']->query("SELECT * FROM( SELECT messages.`to` AS `to`, messages.`time` AS `time`, messages.`message` AS `message`, users.`avatar` AS `avatar`, users.`name` AS `name`, users.`surname` AS `surname` FROM messages INNER JOIN users ON users.`id` = messages.`to` WHERE messages.`from` = {$data['user']->id} ORDER BY messages.`id` DESC) AS x GROUP BY `to`"));
            $messages = array_merge($messagesto, $messagesfrom);
            foreach ($messages AS $key => $value) {
                if (ISSET($value['to'])) {
                    if (ISSET($this->messages[$value['to']]) AND $this->messages[$value['to']]['time'] < $value['time']) {
                        $this->messages[$value['to']] = $value;
                    } else if (!ISSET($this->messages[$value['to']])) {
                        $this->messages[$value['to']] = $value;
                    }
                } else if (ISSET($value['from'])) {
                    if (ISSET($this->messages[$value['from']]) AND $this->messages[$value['from']]['time'] < $value['time']) {
                        $this->messages[$value['from']] = $value;
                    } else if (!ISSET($this->messages[$value['from']])) {
                        $this->messages[$value['from']] = $value;
                    }
                }
            }
            $this->earthspots = query_to_array($data['db']->query("SELECT
                `thinkspot`.*, `photos`.`serv`, `photos`.`file`
                FROM `thinkspot`
                INNER JOIN `photos` ON `photos`.`id` = `thinkspot`.`avatar`
                WHERE `thinkspot`.`author` = {$data['user']->id}"));
        }
    }

    public function index() {
        
    }

}

?>
