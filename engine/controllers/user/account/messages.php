<?php

class Controllers_user_account_messages Extends Controllers_Base {
    var $reqlogged = true;
    var $messages;
    var $frame;

    function __construct() {
        $data = data::init();
        $with = $_GET['with'];
        $this->messages = query_to_array($data['db']->query("
SELECT
	messages.`from` AS `from`,
	messages.`time` AS `time`,
	messages.`message` AS `message`,
        messages.`read` AS `read`,
	users.`avatar` AS `avatar`,
	users.`name` AS `name`,
	users.`surname` AS `surname`
FROM
	messages
INNER JOIN users ON users.`id` = messages.`from`
WHERE
	( messages.`to` = {$data['user']->id} AND messages.`from` = $with) 
OR 
  ( messages.`to` = $with AND messages.`from` = {$data['user']->id})
ORDER BY messages.`time` ASC"));
        $data['db']->query("UPDATE messages SET `read` = 1 WHERE `to` = {$data['user']->id} AND `read` = 0");
    }

    public function index() {
        
    }

}
?>