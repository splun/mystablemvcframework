<?php

Class Controllers_album_photo Extends Controllers_Base {

    var $is = false;
    var $image;
    var $name;
    var $date;
    var $owner;
    var $description;
    var $x_dest;
    var $y_dest;
    var $id;
    var $uid;
    var $frame = false;
    var $comments;
    var $discussed;
    var $comallow;
    var $category;

    function __construct() {
        classOut(__CLASS__);
        if (is_numeric($_GET['id'])) {
            $id = protection::number($_GET['id']);
            $this->id = $id;
            $data = data::init();
            $dateloc = new dateloc();
            $res = $data['db']->query("SELECT photos.*, users.id AS uid, users.name AS uname, users.surname AS usurname FROM photos INNER JOIN users ON photos.owner = users.id WHERE photos.id=$id LIMIT 1");
            if (mysql_num_rows($res)) {
                $res = mysql_fetch_array($res);
                
                
                
                $this->is = true;
                $this->uid = $res['uid'];
                $this->image = $res['serv'] . $res['file'];
                $this->name = $res['name'];
                $this->date = $dateloc->dateTime($res['added']);
                $this->owner = $res['owner'];
                $this->uname = $res['uname'];
                $this->usurname = $res['usurname'];
                $this->category = $res['category'];
                $this->description = $res['description'];
                $this->x_dest = $res['x_dest'];
                $this->y_dest = $res['y_dest'];
                $allowed = $res['allowed'];
                $this->subcats = query_to_array($data['db']->query("SELECT category AS id FROM photos_subcat WHERE photo = '$id'"));
                $show_comment = 1;
                if ($allowed) {
                    $q = "SELECT * FROM `allowed` WHERE `group`=$id AND `user`={$data['user']->id} LIMIT 1";
                    $allow = $data['db']->query($q);
                    if (!mysql_num_rows($allow)) {
                        $show_comment = 0;
                    }
                }
                $this->comallow = $show_comment;
                if ($show_comment) {
                    $msg = array();
                    $comments_list = $data['db']->query("SELECT comments.id, comments.date, comments.text, comments.com_parent, users.id AS unum, users.name, users.surname, users.avatar FROM comments INNER JOIN users ON comments.user = users.id WHERE comments.photo=$id");
                    while ($row = mysql_fetch_assoc($comments_list)) {
                        $msg[] = $row;
                    }
                    $count = count($msg);
                    $parent = 0;
                    $i = 0;
                    if ($count) {
                        $msg = $this->crazysort($msg);
                        while ($i < $count) {
                            $this->comments[$i]['date'] = $dateloc->dateTime($msg[$i]['date']);
                            $this->comments[$i]['id'] = $msg[$i]['id'];
                            $this->comments[$i]['level'] = $msg[$i]['level'];
                            $this->comments[$i]['name'] = $msg[$i]['name'];
                            $this->comments[$i]['unum'] = $msg[$i]['unum'];
                            $this->comments[$i]['surname'] = $msg[$i]['surname'];
                            if ($msg[$i]['avatar'] != '') {
                                $this->comments[$i]['avatar'] = $msg[$i]['avatar'];
                            } else {
                                $this->comments[$i]['avatar'] = 'style/images/avatar.jpg';
                            }
                            $this->comments[$i]['text'] = $msg[$i]['text'];
                            $i++;
                        }
                    }
                }
            } else {
                header("HTTP/1.0 404 Not Found");
            }
            $res = $data['db']->query('SELECT photos.*, COUNT(comments.id) AS com FROM photos LEFT JOIN comments ON comments.photo=photos.id WHERE photos.status=1 GROUP BY photos.id ORDER BY com DESC LIMIT 10');
            if (mysql_num_rows($res)) {
                $res1 = mysql_fetch_array($res);
                $i = 0;
                do {
                    $list[$i]['id'] = $res1['id'];
                    $list[$i]['serv'] = $res1['serv'];
                    $list[$i]['name'] = $res1['name'];
                    $list[$i]['thumb'] = $res1['thumb'];
                    $i++;
                } while ($res1 = mysql_fetch_array($res));
                $this->discussed = $list;
            }
            if (ISSET($_SERVER['HTTP_X_REQUESTED_WITH'])) {
                $this->frame = true;
            }
        }
    }

    function index() {
        
    }

    function crazysort(&$comments, $parentComment = 0, $level = 0, $count = null) {
        if (is_array($comments) && count($comments)) {
            $return = array();
            if (is_null($count)) {
                $c = count($comments);
            } else {
                $c = $count;
            }
            for ($i = 0; $i < $c; $i++) {
                if (!isset($comments[$i]))
                    continue;
                $comment = $comments[$i];
                $parentId = $comment['com_parent'];
                if ($parentId == $parentComment) {
                    $comment['level'] = $level;
                    $commentId = $comment['id'];
                    $return[] = $comment;
                    unset($comments[$i]);
                    while ($nextReturn = $this->crazysort($comments, $commentId, $level + 1, $c)) {
                        $return = array_merge($return, $nextReturn);
                    }
                }
            }
            return $return;
        }
        return false;
    }

}

?>