<?php

Class Controllers_album_list Extends Controllers_Base {

    var $data;

    function __construct() {
        $data = data::init();
        $where = null;
        if (ISSET($_GET['categories'])) {
            $album = protection::number($_GET['categories']);
            $where[] = " `photos`.`category` = {$album} ";
        }
        if (ISSET($_GET['user'])) {
            $user = protection::number($_GET['user']);
            $where[] = " `photos`.`owner` = {$user} ";
        }
        if (ISSET($_GET['x_top'])) {
            $x_top = protection::number($_GET['x_top']);
            $where[] = " `photos`.`x_dest` < {$x_top} ";
        }
        if (ISSET($_GET['x_bottom'])) {
            $x_bottom = protection::number($_GET['x_bottom']);
            $where[] = " `photos`.`x_dest` > {$x_bottom} ";
        }
        if (ISSET($_GET['y_top'])) {
            $y_top = protection::number($_GET['y_top']);
            $where[] = " `photos`.`y_dest` < {$x_top} ";
        }
        if (ISSET($_GET['approved'])) {
            $approved = protection::number($_GET['approved']);
            $where[] = " `photos`.`status` = {$approved} ";
        } else if(ISSET($_POST['approved'])) {
            $approved = protection::number($_POST['approved']);
            $where[] = " `photos`.`status` = {$approved} ";
        } else {
            $where[] = " `photos`.`status` = 1 ";
        }
        if (ISSET($_GET['y_bottom'])) {
            $y_bottom = protection::number($_GET['y_bottom']);
            $where[] = " `photos`.`y_dest` > {$y_bottom} ";
        }
        if (count($where)) {
            $where = ' WHERE ' . implode(' AND ', $where);
        }
        $res = $data['db']->query("SELECT `photos`.*, `users`.`name` AS uname, `users`.`surname` AS usurname FROM photos INNER JOIN users ON `photos`.`owner` = `users`.`id` $where");
        $i = 0;
        $list = array();
        if (mysql_num_rows($res)) {
            $reslist = mysql_fetch_array($res);
            $cat = new categories();
            $dateloc = new dateloc();
            do {
                $ct['id'] = $reslist['category'];
                $ct['name'] = $cat->getName($reslist['category']);
                $list[$i]['x'] = $reslist['x_dest'];
                $list[$i]['y'] = $reslist['y_dest'];
                $list[$i]['serv'] = $reslist['serv'];
                $list[$i]['added'] = $dateloc->dateTime($reslist['added']);
                $list[$i]['thumb'] = $reslist['thumb'];
                $list[$i]['file'] = $reslist['file'];
                $list[$i]['owner'] = $reslist['owner'];
                if((strlen($reslist['uname']) < 1) AND (strlen($reslist['usurname']) < 1)){
                    $list[$i]['usurname'] = 'Noname';
                }
                $list[$i]['uname'] = $reslist['uname'];
                $list[$i]['usurname'] = $reslist['usurname'];
                $list[$i]['id'] = $reslist['id'];
                $list[$i]['name'] = $reslist['name'];
                if(strlen($reslist['description']) > 0){
                    $list[$i]['description'] = $reslist['description'];
                } else {
                    $list[$i]['description'] = '';
                }                
                $list[$i]['time'] = dateloc::dateTime($reslist['added']);
                $list[$i]['subcat'][] = $ct;
                $subcats = query_to_array($data['db']->query("SELECT category AS id FROM photos_subcat WHERE photo = '{$reslist['id']}'"));
                foreach($subcats AS $subcat){
                    $subcat['name'] = $cat->getName($subcat['id']);
                    $list[$i]['subcat'][] = $subcat;
                }
                $i++;
            } while ($reslist = mysql_fetch_array($res));
        }
        die(json_encode(array('markers' => $list)));
    }

    public function index() {
        
    }

}

?>