<?php

Class Controllers_album_think Extends Controllers_Base {

    function __construct() {
        classOut(__CLASS__);
        $data = data::init();
        $cat = new categories();
        if (ISSET($_GET['cat'])) {
            $id = 0;
            if (is_numeric($_GET['cat'])) {
                $id = $_GET['cat'];
            }
            $name = $cat->getName($id);
            $info = null;
            $re = $data['db']->query("SELECT
                `thinkspot`.*, `users`.`name` AS uname, `users`.`surname` AS usurname,
                `photos`.`serv`, `photos`.`file`
                FROM `thinkspot` INNER JOIN `users` ON `thinkspot`.`author` = `users`.`id`
                INNER JOIN `photos` ON `photos`.`id` = `thinkspot`.`avatar`
                WHERE `thinkspot`.`category` = {$id} LIMIT 18");
            if (mysql_num_rows($re)) {
                $res = query_to_array($re);
                $ct['id'] = $id;
                $ct['name'] = $name;
                $ct['photos'] = $res;
                $info[] = $ct;
            }
            $this->info = $info;
        } else {
            $cats = $cat->getAll();
            $info = null;
            foreach ($cats AS $category) {
                $re = $data['db']->query("SELECT
                `thinkspot`.*, `users`.`name` AS uname, `users`.`surname` AS usurname,
                `photos`.`serv`, `photos`.`file`
                FROM `thinkspot` INNER JOIN `users` ON `thinkspot`.`author` = `users`.`id`
                INNER JOIN `photos` ON `photos`.`id` = `thinkspot`.`avatar`
                WHERE `thinkspot`.`category` = {$category['id']} LIMIT 3");
                if (mysql_num_rows($re)) {
                    $res = query_to_array($re);
                    $ct['id'] = $category['id'];
                    $ct['name'] = $category['name'];
                    $ct['photos'] = $res;
                    $info[] = $ct;
                }
            }
            $this->info = $info;
        }
    }

    function index() {
        
    }

}

?>