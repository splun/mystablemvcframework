<?php
spl_autoload_register('loadBase');
spl_autoload_register('loadClass');
spl_autoload_register('loadView');
spl_autoload_register('loadController');
function loadBase($controller){
    $controller = str_replace('_',DS,strtolower($controller));
    $filename = PATH_CORE.$controller.'.php';
    if(file_exists($filename)){
       include($filename);
       return true;
    }
    return false;
}
function loadClass($classname){
    $classname = str_replace('_',DS,strtolower($classname));
    $filename = PATH_CORE.'classes'.DS.$classname.'.php';
    if(file_exists($filename)){
       include($filename);
       return true;
    }
    return false;
}
function loadView($view){
    $data = data::init();
    $view = str_replace('_',DS,strtolower($view));
    $filename = PATH_CORE.'views'.DS.$view.'.php';
    if(file_exists($filename)){
       include($filename);
       return true;
    }
    return false;
}
function loadController($controller){
    $controller = str_replace('_',DS,strtolower($controller));
    $filename = PATH_CORE.'controllers'.DS.$controller.'.php';
    if(file_exists($filename)){
       include($filename);
       return true;
    }
    return false;
}
?>