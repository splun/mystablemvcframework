var m_strUpperCase = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
var m_strLowerCase = "abcdefghijklmnopqrstuvwxyz";
var m_strNumber = "0123456789";
var m_strCharacters = "!@#$%^&*?_~"
function checkPassword(strPassword)
{
    var nScore = 0;
    if (strPassword.length < 5) { nScore += 5; }
    else if (strPassword.length > 4 && strPassword.length < 8) { nScore += 10; }
    else if (strPassword.length > 7 && strPassword.length < 13) { nScore += 15; }
    else if (strPassword.length > 12 && strPassword.length < 15) { nScore += 20; }
    else if (strPassword.length > 14) { nScore += 25; }
    var nUpperCount = countContain(strPassword, m_strUpperCase);
    var nLowerCount = countContain(strPassword, m_strLowerCase);
    var nNumberCount = countContain(strPassword, m_strNumber);
    var nCharacterCount = countContain(strPassword, m_strCharacters);
    var nLowerUpperCount = nUpperCount + nLowerCount;
    if (nLowerCount != 0) { nScore += 10; }
    if (nUpperCount != 0) { nScore += 20; }
    if (nNumberCount == 1) { nScore += 10; }
    if (nNumberCount > 2) { nScore += 20; }
    if (nCharacterCount == 1) { nScore += 10; }	
    if (nCharacterCount > 1) { nScore += 25; }
    if (nNumberCount != 0 && nLowerUpperCount != 0) { nScore += 2; }
    if (nNumberCount != 0 && nLowerUpperCount != 0 && nCharacterCount != 0) { nScore += 3; }
    if (nNumberCount != 0 && nUpperCount != 0 && nLowerCount != 0 && nCharacterCount != 0) { nScore += 5; }
    return nScore;
}

function runPassword(strPassword, strFieldID) 
{
    var nScore = checkPassword(strPassword);
    var ctlBar = document.getElementById(strFieldID); 
    var ctlText = document.getElementById(strFieldID + "_text");
    if (!ctlBar || !ctlText) return;
    if (nScore >= 90)
    {
        var strText = "Super!";
        var strColor = "#0ca908";
    }
    else if (nScore >= 80)
    {
        var strText = "Very good";
        vstrColor = "#7ff67c";
    }
    else if (nScore >= 70)
    {
        var strText = "Good";
        var strColor = "#1740ef";
    }
    else if (nScore >= 60)
    {
        var strText = "Normal";
        var strColor = "#5a74e3";
    }
    else if (nScore >= 50)
    {
        var strText = "Normal";
        var strColor = "#e3cb00";
    }
    else if (nScore >= 25)
    {
        var strText = "Easy";
        var strColor = "#e7d61a";
    }
    else
    {
        var strText = "Very easy";
        var strColor = "#e71a1a";
    }
    ctlText.style.backgroundColor = strColor;
    ctlText.innerHTML = strText;
}
function countContain(strPassword, strCheck) {
    var nCount = 0;
    for (i = 0; i < strPassword.length; i++) 
    {
        if (strCheck.indexOf(strPassword.charAt(i)) > -1) 
        { 
            nCount++;
        } 
    }
    return nCount; 
}