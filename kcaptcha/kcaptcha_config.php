<?php
$alphabet = "0123456789abcdefghijklmnopqrstuvwxyz"; # do not change without changing font files!

# symbols used to draw CAPTCHA
//$allowed_symbols = "0123456789"; #digits
//$allowed_symbols = "23456789abcdegkmnpqsuvxyz"; #alphabet without similar symbols (o=0, 1=l, i=j, t=f)
$allowed_symbols = "23456789abcdegikpqsvxyz"; #alphabet without similar symbols (o=0, 1=l, i=j, t=f)
$fontsdir = 'fonts';
$length = mt_rand(6,8);
$width = 160;
$height = 80;
$fluctuation_amplitude = 2;
$white_noise_density = 0;
$black_noise_density = 0;
$no_spaces = true;
$show_credits = false;
$credits = 'Earthinks';
$foreground_color = array(mt_rand(0,80), mt_rand(0,80), mt_rand(0,80));
//$background_color = array(mt_rand(220,255), mt_rand(220,255), mt_rand(220,255));
$background_color = array(255,255,255);
$jpeg_quality = 100;
?>