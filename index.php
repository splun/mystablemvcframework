<?php
ob_start();
session_start();
define('DS', DIRECTORY_SEPARATOR);
define('PATH_ROOT', dirname(__FILE__) . DS);
require_once 'engine'.DS.'defines.php';
require_once 'engine'.DS.'core.php';
$final = ob_get_contents();
ob_end_clean();
echo $final;
?>